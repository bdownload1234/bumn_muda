<?= $this->include('includes/subscribe') ?>

<footer class="footer bg-white min-height-490 padding-y-100" id="section-footer">
  <div class="container">
    <div class="footer-wrapper padding-bottom-48">
      <div class="col-md-12 footer-flex align-items-center">
        <div class="col-md-4 d-flex flex-wrap footer-child">
          <div class="footer-logo">
            <img src="<?= base_url() ?>/assets/img/globals/logo-white.svg" alt="" class="img-fluid" width="220" height="auto">
          </div>
        </div>
        <div class="col-md-4 d-flex flex-wrap footer-child">
          <p class="text-dark text-opacity-50 mb-0 font-roboto font-light font-18">
            Gd. Plaza Mandiri lt.28
          </p>
          <p class="text-dark text-opacity-50 mb-0 font-roboto font-light font-18">
            021 27095432 | fhcibumn@gmail.com
          </p>
        </div>
        <div class="col-md-4 d-flex flex-wrap footer-child">
          <div class="footer-social-media">
            <div class="d-flex justify-content-between">
              <div class="col-md-2 p-2">
                <a href="javascript:;" class="text-dark text-opacity-50 text-decoration-none text-transform-uppercase font-roboto font-regular font-16">
                  Twitter
                </a>
              </div>
              <div class="col-md-2 p-2">
                <a href="javascript:;" class="text-dark text-opacity-50 text-decoration-none text-transform-uppercase font-roboto font-regular font-16">
                  Facebook
                </a>
              </div>
              <div class="col-md-2 p-2">
                <a href="javascript:;" class="text-dark text-opacity-50 text-decoration-none text-transform-uppercase font-roboto font-regular font-16">
                  Instagram
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <hr class="opacity-25">
    <div class="footer-copy text-center padding-y-32">
      <span class="text-dark text-opacity-50 font-roboto font-18 font-light">© 2019 by Forum Human Capital Indonesia. All Rights Reserved.</span>
    </div>
  </div>
</footer>