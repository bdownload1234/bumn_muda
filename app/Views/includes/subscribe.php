<section class="bg-primary padding-y-100" id="section-subscribe">
  <div class="container">
    <div class="section-wrapper">
      <div class="section-subscribe d-flex align-items-center justify-content-center">
        <h6 class="font-28 text-white mb-0 mx-3">Ingin Mendapatkan Update Terbaru mengenai FHCI?</h6>
        <a href="javascript:;" data-bs-toggle="modal" data-bs-target="#showSubscribeForm" class="btn btn-outline-light rounded-pill text-transform-uppercase padding-y-16 padding-x-64 mx-3">
          Subscribe
        </a>
      </div>
    </div>
  </div>
</section>

<!-- Modal -->
<div class="modal fade bg-white bg-opacity-50" id="showSubscribeForm" data-bs-keyboard="true" tabindex="-1" aria-labelledby="showSubscribeFormLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content p-0 rounded-0 border-0 bg-transparent">
      <div class="modal-body p-0 border-0">
        <div class="padding-x-36 padding-y-36 bg-white text-center rounded">
          <h5 class="font-24">Gabung bersama kami untuk mengetahui info terbaru FHCI</h5>
          <hr width="50" class="mx-auto margin-bottom-18">
          <form action="#" method="POST" id="form_subscribe">
            <div class="form-group margin-y-16">
              <input type="text" class="form-control padding-x-18 padding-y-16" id="subscribe_name" name="subscribe_name" placeholder="Nama Anda">
            </div>
            <div class="form-group margin-y-16">
              <input type="text" class="form-control padding-x-18 padding-y-16" id="subscribe_email" name="subscribe_email" placeholder="Alamat Surel Anda">
            </div>
            <div class="form-group">
              <button type="submit" class="btn btn-primary btn-block padding-x-24 padding-y-14">
                Subscribe
                <i class="bi bi-bell margin-left-4"></i>
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- End Modal -->

<?= $this->section('append-script') ?>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script>
  $('#form_subscribe').on('submit', function(e) {
    e.preventDefault();
    $.ajax({
      url: '<?= base_url() ?>/subscribe',
      type: 'POST',
      data: $(this).serialize(),
      success: function(response) {
        if (response.status) {
          $('#showSubscribeForm').modal('hide');
          $('#form_subscribe')[0].reset();
          Swal.fire({
            title: 'Berhasil',
            text: 'Berhasil melakukan subscribe',
            type: 'success',
            confirmButtonText: 'Ok'
          });
        } else {
          Swal.fire({
            title: 'Gagal',
            text: response.message,
            type: 'error',
            confirmButtonText: 'Ok'
          });
        }
      }
    });
  });
</script>
<?= $this->endSection() ?>