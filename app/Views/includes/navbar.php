

<nav class="navbar navbar-expand-lg fixed-top">
  <div class="container">
    <a class="navbar-brand" href="<?= base_url() ?>">
      <img src="<?= current_url() == base_url('/') ? base_url('/') . '/assets/img/globals/logo-white.svg' : base_url('/') . '/assets/img/globals/logo-color.svg' ?>" alt="" width="auto" height="74px">
    </a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
      <div class="navbar-nav ms-auto">
        <a class="nav-link <?= current_url() == base_url('/') . '/' ? 'active' : '' ?>" href="<?= base_url() ?>">Home</a>
        <div class="dropdown has-submenu">
          <a class="nav-link dropdown-toggle" href="javascript:;" id="media-center" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            Tentang Kami
          </a>
          <ul class="dropdown-menu" aria-labelledby="media-center">
            <a class="dropdown-item <?= current_url() == base_url('/') . '/tentang-kami/sejarah-dan-tujuan' ? 'active' : '' ?>" href="<?= base_url() ?>/tentang-kami/sejarah-dan-tujuan">Sejarah dan Tujuan</a>
            <a class="dropdown-item <?= current_url() == base_url('/') . '/tentang-kami/arti-logo' ? 'active' : '' ?>" href="<?= base_url() ?>/tentang-kami/arti-logo">Arti Logo</a>
            <a class="dropdown-item <?= current_url() == base_url('/') . '/tentang-kami/founder-dan-pengurus' ? 'active' : '' ?>" href="<?= base_url() ?>/tentang-kami/founder-dan-pengurus">Founder dan Pengurus</a>
          </ul>
        </div>
        <div class="dropdown has-submenu">
          <a class="nav-link dropdown-toggle" href="javascript:;" id="media-center" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            Program
          </a>
          <ul class="dropdown-menu" aria-labelledby="media-center">
            <a class="dropdown-item <?= current_url() == base_url('/') . '/program/kajian-dan-pendampingan-perempuan' ? 'active' : '' ?>" href="<?= base_url() ?>/program/kajian-dan-pendampingan-perempuan">Kajian dan Pendampingan Perempuan</a>
            <a class="dropdown-item <?= current_url() == base_url('/') . '/program/pendidikan-dan-pengembangan' ? 'active' : '' ?>" href="<?= base_url() ?>/program/pendidikan-dan-pengembangan">Pendidikan dan Pengembangan</a>
            <a class="dropdown-item <?= current_url() == base_url('/') . '/program/kesehatan-dan-kesejahteraan' ? 'active' : '' ?>" href="<?= base_url() ?>/program/kesehatan-dan-kesejahteraan">Kesehatan dan Kesejahteraan</a>
            <a class="dropdown-item <?= current_url() == base_url('/') . '/program/bidang-komunikasi-dan-kerjasama' ? 'active' : '' ?>" href="<?= base_url() ?>/program/bidang-komunikasi-dan-kerjasama">Bidang Komunikasi dan kerjasama</a>
            <a class="dropdown-item <?= current_url() == base_url('/') . '/program/sosial-masyarakat' ? 'active' : '' ?>" href="<?= base_url() ?>/program/sosial-masyarakat">Sosial Masyarakat</a>
          </ul>
        </div>
        <div class="dropdown has-submenu">
          <a class="nav-link dropdown-toggle" href="javascript:;" id="media-center" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            Berita
          </a>
          <ul class="dropdown-menu" aria-labelledby="media-center">
            <a class="dropdown-item <?= current_url() == base_url('/') . '/berita' ? 'active' : '' ?>" href="<?= base_url() ?>/berita">Berita</a>
            <a class="dropdown-item <?= current_url() == base_url('/') . '/berita/artikel-dan-event' ? 'active' : '' ?>" href="<?= base_url() ?>/berita/artikel-dan-event">Artikel dan Event</a>
            <a class="dropdown-item <?= current_url() == base_url('/') . '/berita/figur' ? 'active' : '' ?>" href="<?= base_url() ?>/berita/figur">Figur</a>
          </ul>
        </div>
        <a class="nav-link <?= current_url() == base_url('/') . '/galeri' ? 'active' : '' ?>" href="<?= base_url() ?>/galeri">Galeri</a>
        <a class="nav-link <?= current_url() == base_url('/') . '/kontak' ? 'active' : '' ?>" href="<?= base_url() ?>/kontak">Kontak</a>
        <a class="btn text-white btn-secondary padding-left-36 padding-right-22 margin-left-32 rounded-pill" id="button-fhci" href="#">FHCI <i class="margin-left-16 bi bi-arrow-right-short"></i></a>
      </div>
    </div>
  </div>
</nav>