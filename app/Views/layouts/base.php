<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>BUMN Muda</title>
  <?= $this->renderSection('prepend-style') ?>
  <link rel="stylesheet" href="<?= base_url() ?>/assets/vendor/bootstrap-icons/font/bootstrap-icons.css">
  <link rel="stylesheet" href="<?= base_url() ?>/assets/vendor/swiperjs/css/swiper-bundle.min.css">
  <link rel="stylesheet" href="<?= base_url() ?>/assets/css/app.css">
  <?= $this->renderSection('append-style') ?>
</head>
<body class="overflow-x-hidden">

  <?= $this->include('includes/navbar') ?>

  <?= $this->renderSection('content') ?>

  <?= $this->include('includes/footer') ?>
  
  <?= $this->renderSection('prepend-script') ?>
  <script src="<?= base_url() ?>/assets/vendor/jquery/jquery-3.6.0.js"></script>
  <script src="<?= base_url() ?>/assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="<?= base_url() ?>/assets/vendor/swiperjs/js/swiper-bundle.min.js"></script>
  <script src="<?= base_url() ?>/assets/js/app.js"></script>
  <?= $this->renderSection('append-script') ?>
</body>
</html>