<?= $this->extend('layouts/base') ?>

<?= $this->section('append-style') ?>
<link rel="stylesheet" href="<?= base_url() ?>/assets/css/pages/index-responsive.css">
<link rel="stylesheet" href="<?= base_url() ?>/assets/css/pages/index.css">
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<section id="banner" class="margin-top-100">
  <div class="banner-swiper swiper">
    <div class="banner-wrapper swiper-wrapper">
      <div class="banner-slide swiper-slide">
        <div class="banner-media">
          <img src="assets/img/banners/banner-1.png" alt="" height="100%" width="100%">
        </div>
        <div class="banner-content container">
          <div class="row w-100">
            <div class="col-lg-6">
              <div class="banner-header">
                <h1 class="font-65 font-md-39 text-white font-regular">Gerakan<br>Saya Berani</h1>
              </div>
              <div class="banner-subheader">
                <p class="font-roboto text-white font-24 font-md-18 font-light">
                  Gerakan saya berani yang diprakarsai oleh Srikandi BUMN pada tahun 2021
                </p>
              </div>
              <div class="banner-action">
                <a href="#" class="btn btn-outline-light font-semi-bold font-16 padding-y-16 padding-x-36 rounded-pill">
                  Lebih Lanjut
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="section padding-y-64" id="section-what-is-srikandi">
    <div class="container container-no-xl">
      <div id="srikandi-explanation">
        <div class="row m-0">
          <div class="col-lg-6 p-0">
            <div class="section-image p-relative d-flex justify-content-end padding-y-128">
              <img src="assets/img/sections/section-1.jpg" alt="" width="auto" height="100%">
              <div class="section-play-video">
                <a href="javascript:;" data-bs-toggle="modal" data-bs-target="#playVideo" class="d-block">
                  <img src="assets/img/globals/play.svg" alt="" class="img-fluid">
                </a>
              </div>
            </div>
          </div>
          <div class="col-lg p-0">
            <div class="section-content bg-srikandi-icon-half h-100 d-flex align-items-center padding-y-256 padding-left-128 padding-right-192 padding-bottom-48 padding-x-md-48 padding-top-md-0 padding-bottom-md-0">
              <div class="section-contet-wrapper">
                <div class="section-content-header">
                  <h5 class="font-bold font-40 font-md-26">Apa itu Srikandi BUMN?</h5>
                </div>
                <div class="section-content-body margin-top-32">
                  <p class="font-18 line-height-33 font-md-16 font-regular">
                    Srikandi BUMN merupakan komunitas perempuan berkarya di BUMN untuk saling mendukung sesama perempuan dalam berkarya dan berprestasi dengan beragam peran sebagai ibu, istri dan pekerja. Komunitas Srikandi BUMN didirikan untuk menjawab panggilan Bpk. Joko Widodo, Presiden Republik Indonesia yang juga Impact Champion for HeforShe Program United Nations, yang menyatakan bahwa Indonesia berkomitmen untuk meningkatkan keterwakilan perempuan hingga 30% di parlemen dan berbagai pembuat kebijakan.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="container p-0" id="srikandi-about">
        <div class="row m-0 flex-row-reverse">
          <div class="col-lg-6 p-0">
            <div class="section-image p-relative d-flex justify-content-start padding-y-128">
              <img src="assets/img/sections/about-1.jpg" alt="" class="img-fluid padding-left-48 w-100" height="100%">
              <img src="assets/img/sections/about-1-before.jpg" alt="" class="img-fluid w-100 padding-right-48 margin-top-48" height="100%">
            </div>
          </div>
          <div class="col-lg p-0">
            <div class="section-content bg-srikandi-icon-half h-100 padding-x-48 d-flex align-items-center">
              <div class="section-contet-wrapper">
                <div class="section-content-header">
                  <h5 class="font-bold font-40 font-md-26">Tentang Srikandi<br>BUMN</h5>
                </div>
                <div class="section-content-body margin-top-48">
                  <div class="content-item d-flex margin-top-36">
                    <div class="content-icon margin-right-16 margin-top-4">
                      <img src="assets/img/misc/sejarah-icon.svg" alt="" width="18">
                    </div>
                    <div class="content-text">
                      <h6 class="font-18 font-roboto fonr-bold line-height-33">Sejarah</h6>
                      <p class="font-18 line-height-33 font-md-16 font-regular">
                        Komunitas perempuan berkarya di BUMN untuk saling mendukung dalam berkarya dan berprestasi dengan beragam peran, baik sebagai ibu, istri, maupun pekerja.
                      </p>
                      <a href="#" class="btn btn-link text-primary text-decoration-none text-transform-uppercase font-semi-bold font-16 p-0">
                        Lebih lanjut
                        <i class="bi bi-arrow-right-short margin-left-8"></i>
                      </a>
                    </div>
                  </div>
                  <div class="content-item d-flex margin-top-36">
                    <div class="content-icon margin-right-16 margin-top-4">
                      <img src="assets/img/misc/tujuan-icon.svg" alt="" width="18">
                    </div>
                    <div class="content-text">
                      <h6 class="font-18 font-roboto fonr-bold line-height-33">Tujuan</h6>
                      <p class="font-18 line-height-33 font-md-16 font-regular">
                        Memberikan wadah bagi perempuan berkarya di BUMN agar dapat saling mendukung, membangun kapasitas, mampu beradaptasi, serta menjaga keseimbangan perannya
                      </p>
                      <a href="#" class="btn btn-link text-primary text-decoration-none text-transform-uppercase font-semi-bold font-16 p-0">
                        Lebih lanjut
                        <i class="bi bi-arrow-right-short margin-left-8"></i>
                      </a>
                    </div>
                  </div>
                  <div class="content-item d-flex margin-top-36">
                    <div class="content-icon margin-right-16 margin-top-4">
                      <img src="assets/img/misc/pengurus-icon.svg" alt="" width="18">
                    </div>
                    <div class="content-text">
                      <h6 class="font-18 font-roboto fonr-bold line-height-33">Pengurus</h6>
                      <p class="font-18 line-height-33 font-md-16 font-regular">
                        Pengukuhan Ketua Srikandi BUMN, Tina Kemala Intan, sekaligus pengurus periode 2021-2024 dilakukan pada 7 April 2021 oleh Menteri BUMN Erick Thohir.
                      </p>
                      <a href="#" class="btn btn-link text-primary text-decoration-none text-transform-uppercase font-semi-bold font-16 p-0">
                        Lebih lanjut
                        <i class="bi bi-arrow-right-short margin-left-8"></i>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal -->
    <div class="modal fade bg-white bg-opacity-50" id="playVideo" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="playVideoLabel" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content p-0 rounded-0 border-0 bg-transparent">
          <button type="button" class="btn-close border border-3 border-dark p-2 ms-auto mb-2 opacity-100" data-bs-dismiss="modal" aria-label="Close"></button>
          <div class="modal-body p-0 border-0">
            <video src="assets/vid/banner-2.mp4" control="false" class="w-100 h-100 object-fit-cover"></video>
          </div>
        </div>
      </div>
    </div>
    <!-- End Modal -->
  </section>

<section class="section min-height-1000 bg-light padding-top-100 padding-bottom-156" id="section-program-unggulan">
  <div class="container">
    <div class="section-header text-center margin-bottom-100">
      <h5 class="font-medium font-40 font-md-26">Program Unggulan</h5>
      <p class="font-roboto font-light font-23 margin-top-23">Program Unggulan yang tedapat dalam Srikandi BUMN</p>
    </div>
  </div>
  <div class="section-swiper swiper">
    <div class="section-wrapper swiper-wrapper">
      <div class="section-slide swiper-slide program-unggulan bg-primary text-white h-auto row flex-column justify-content-between">
        <div class="section-content-wrapper">
          <div class="section-slide-header text-center">
            <div class="slide-header-media padding-top-100 padding-top-md-20 padding-bottom-64 padding-bottom-md-20">
              <img src="assets/img/misc/prog-1.svg" alt="" height="auto" width="auto">
            </div>
            <div class="slide-header-text">
              <h6 class="font-roboto font-md-16 font-23 font-bold margin-bottom-22">Kajian & Pendampingan</br>Perempuan</h6>
            </div>
          </div>
          <div class="section-slide-body padding-x-24 padding-x-md-18 text-center margin-bottom-24">
            <p class="font-20 font-md-14 font-roboto font-regular">Mengkaji isu perempuan, termasuk
              mencari langkah terbaik di lingkungan
              BUMN, untuk mendukung kebijakan
              bidang perempuan.</p>
          </div>
        </div>
      </div>
      <div class="section-slide swiper-slide program-unggulan bg-primary text-white h-auto row flex-column justify-content-between">
        <div class="section-content-wrapper">
          <div class="section-slide-header text-center">
            <div class="slide-header-media padding-top-100 padding-top-md-20 padding-bottom-64 padding-bottom-md-20">
              <img src="assets/img/misc/prog-2.svg" alt="" height="auto" width="auto">
            </div>
            <div class="slide-header-text">
              <h6 class="font-roboto font-md-16 font-23 font-bold margin-bottom-24">Pendidikan &<br>Pengembangan</h6>
            </div>
          </div>
          <div class="section-slide-body padding-x-24 padding-x-md-18 text-center margin-bottom-24">
            <p class="font-20 font-md-14 font-roboto font-regular">Menyelenggarakan kegiatan terkait Pendidikan untuk perempuan dan pengembangan pemimpin perempuan.</p>
          </div>
        </div>
      </div>
      <div class="section-slide swiper-slide program-unggulan bg-primary text-white h-auto row flex-column justify-content-between">
        <div class="section-content-wrapper">
          <div class="section-slide-header text-center">
            <div class="slide-header-media padding-top-100 padding-top-md-20 padding-bottom-64 padding-bottom-md-20">
              <img src="assets/img/misc/prog-3.svg" alt="" height="auto" width="auto">
            </div>
            <div class="slide-header-text">
              <h6 class="font-roboto font-md-16 font-23 font-bold margin-bottom-24">Kesehatan &<br>Kesejahteraan</h6>
            </div>
          </div>
          <div class="section-slide-body padding-x-24 padding-x-md-18 text-center margin-bottom-24">
            <p class="font-20 font-md-14 font-roboto font-regular">Mengambil bagian dalam meningkatkan kesehatan diri, keluarga, dan lingkungan untuk Indonesia yang lebih sehat.</p>
          </div>
        </div>
      </div>
      <div class="section-slide swiper-slide program-unggulan bg-primary text-white h-auto row flex-column justify-content-between">
        <div class="section-content-wrapper">
          <div class="section-slide-header text-center">
            <div class="slide-header-media padding-top-100 padding-top-md-20 padding-bottom-64 padding-bottom-md-20">
              <img src="assets/img/misc/prog-4.svg" alt="" height="auto" width="auto">
            </div>
            <div class="slide-header-text">
              <h6 class="font-roboto font-md-16 font-23 font-bold margin-bottom-24">Komunikasi &<br>Kerjasama</h6>
            </div>
          </div>
          <div class="section-slide-body padding-x-24 padding-x-md-18 text-center margin-bottom-24">
            <p class="font-20 font-md-14 font-roboto font-regular">Mendukung efektifitas organisasi Srikandi dengan aktivitas komunikasi eksternal sekaligus membangun partnership yang menguntungkan komunitas.</p>
          </div>
        </div>
      </div>
      <div class="section-slide swiper-slide program-unggulan bg-primary text-white h-auto row flex-column justify-content-between">
        <div class="section-content-wrapper">
          <div class="section-slide-header text-center">
            <div class="slide-header-media padding-top-100 padding-top-md-20 padding-bottom-64 padding-bottom-md-20">
              <img src="assets/img/misc/prog-5.svg" alt="" height="auto" width="auto">
            </div>
            <div class="slide-header-text">
              <h6 class="font-roboto font-md-16 font-23 font-bold margin-bottom-24">Sosial<br>Masyarakat</h6>
            </div>
          </div>
          <div class="section-slide-body padding-x-24 padding-x-md-18 text-center margin-bottom-24">
            <p class="font-20 font-md-14 font-roboto font-regular">Membangun kegiatan sosial kemasyarakatan untuk meningkatkan kesadaran masyarakat tentang kesetaraan gender dan hak perempuan.</p>
          </div>
        </div>
      </div>
    </div>
    <div class="swiper-button-next section-navigation-next"></div>
    <div class="swiper-button-prev section-navigation-prev"></div>
  </div>
</section>

<section class="section min-height-500 padding-y-90" id="section-page-event">
  <div class="event-wrapper">
    <div class="container">
      <div class="event-header text-center">
        <h5 class="font-40 font-medium">Calendar of Event Srikandi BUMN</h5>
        <div class="event-header-search d-flex align-items-center justify-content-center margin-top-64">
          <form action="#" method="POST" id="form_search-event">
            <div class="d-flex flex-wrap margin-x-16-negative align-items-center">
              <div class="form-group option-bumn padding-x-16">
                <div class="d-flex border-bottom align-items-center">
                  <img src="assets/img/misc/calendar-icon.svg" alt="" width="18" class="me-1">
                  <select name="event-date" class="form-select select-bumn border-0 rounded-0 padding-y-16">
                    <option value="">Pilih Tanggal</option>
                    <?php foreach ($events as $key_event => $value_event): ?>
                      <option value="<?= $value_event->date ?>"><?= date('d M Y', strtotime($value_event->date)) ?></option>
                    <?php endforeach ?>
                  </select>  
                </div>
              </div>              
              <div class="form-group event-bumn padding-x-16 h-100">
                <div class="d-flex border-bottom align-items-center">
                  <img src="assets/img/misc/search-icon.svg" alt="" width="18" class="me-1">
                  <input name="event-name" type="text" class="form-control search-event h-100 border-0 rounded-0 padding-y-16" placeholder="Cari Event">
                </div>
              </div>
              <div class="form-group submit-event padding-x-16">
                <button type="submit" class="btn btn-outline-dark rounded-pill submit-event-bumn padding-x-36 padding-y-16">
                  Cari Event
                </button>
              </div>
            </div>
          </form>
        </div>
      </div>
      <div class="event-body margin-top-148">
        <div class="row">
          <?php foreach($events as $key_event => $value_event): ?>
            <div class="col-xl-3 col-lg-4 col-md-6">
              <div class="event-item-wrapper min-height-200" data-date="<?= $value_event->date ?>">
                <div class="item-wrapper-inner">
                  <div class="item-image">
                    <img src="assets/img/sections/evt-1.jpg" alt="" class="w-100">
                  </div>
                  <div class="item-title padding-x-24 padding-y-36 font-23 font-light text-white">
                    <span>
                      <?= $value_event->title ?>
                    </span>
                  </div>
                </div>
                <div class="item-badge bg-primary padding-x-8 padding-y-16 text-center">
                  <p class="text-white mb-0">
                    <span class="font-28 font-bold line-height-36">
                      <?= date('d', strtotime($value_event->date)) ?>
                    </span>
                    <span class="font-regular line-height-22 font-18">
                      <?= strtoupper(date('M', strtotime($value_event->date))) ?>
                      <br>
                      <?= date('Y', strtotime($value_event->date)) ?>
                    </span>
                  </p>
                </div>
              </div>
            </div>
          <?php endforeach; ?>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="section min-height-900 padding-y-64 bg-light" id="section-blog">
  <div class="container">
    <div class="section-header text-center">
      <h5 class="font-40 font-medium">Srikandi Dalam Berita</h5>
      <p class="font-roboto font-23 font-light margin-top-22">Berita dan Release Terbaru</p>
    </div>
    <div class="section-body margin-top-64">
      <div class="section-wrapper">
        <div class="blog-wrapper">
          <div class="row">
            <?php foreach($blogs as $blog_key => $blog_item): ?>
              <div class="col-md-3">
                <div class="blog-wrapper-inner bg-white h-100 d-flex flex-column margin-bottom-32 justify-content-between">
                  <div class="blog-group">
                    <div class="blog-header">
                      <div class="blog-image overflow-hidden text-center" style="height: 200px;">
                        <img src="<?= API_URL . '/assets/images/' . $blog_item->thumbnail?>" alt="" class="h-100 w-100" style="object-fit: cover;">
                      </div>
                    </div>
                    <div class="blog-body padding-x-22 min-height-180 max-height-180">
                      <div class="blog-title padding-top-22">
                        <a href="#" class="btn-link text-decoration-none text-dark">
                          <h6 class="font-18 font-roboto font-medium">
                            <?php
                              $title = $blog_item->title;
                              if(strlen($title) > 50) {
                                $title = substr($title, 0, 50) . '...';
                              }
                              echo $title;
                            ?>
                          </h6>
                        </a>
                      </div>
                      <div class="blog-meta margin-top-12 ">
                        <p class="font-15 font-roboto font-regular line-height-20 opacity-50">
                        <?php
                          $content = $blog_item->content;
                          if(strlen($content) > 150) {
                            $content = substr($content, 0, 150) . '...';
                          }
                          echo strip_tags($content);
                        ?>
                        </p>
                      </div>
                    </div>
                  </div>
                  <div class="blog-footer padding-x-22 padding-y-16 padding-bottom-22 border-top">
                    <span class="font-12 font-roboto font-regular text-transform-uppercase opacity-50">
                      <?= date('d M Y', strtotime($blog_item->created_at)) ?>
                    </span>
                  </div>
                </div>
              </div>
            <?php endforeach; ?>
          </div>
        </div>
        <div class="section-cta-load text-center margin-top-64">
          <a href="#" class="btn btn-outline-secondary rounded-pill padding-x-48 padding-y-18 font-18 font-regular font-roboto">
            Load More
          </a>
        </div>
      </div>
    </div> 
  </div>
</section>
<?= $this->endSection() ?>

<?= $this->section('prepend-script') ?>
<script>
  const page = 'index';
</script>
<?= $this->endSection() ?>

<?= $this->section('append-script') ?>
<script>
  $("#form_search-event").on("submit", function(e) {
    e.preventDefault();
    var date = $(this).find("select[name='event-date']").val();
    var name = $(this).find("input[name='event-name']").val();
    $(".event-body .row .event-item-wrapper").filter(function() {
      $(this).toggle(($(this).data('date').toLowerCase().indexOf(date) > -1 && date != '')|| ($(this).text().toLowerCase().indexOf(name) > -1 && name != ''));
    });
  });
</script>
<?= $this->endSection() ?>