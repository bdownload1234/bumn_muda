<?= $this->extend('layouts/base') ?>

<?= $this->section('prepend-style') ?>
<?= $this->endSection() ?>

<?= $this->section('append-style') ?>
<link rel="stylesheet" href="<?= base_url() ?>/assets/css/pages/program-responsive.css">
<link rel="stylesheet" href="<?= base_url() ?>/assets/css/pages/program.css">
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<section class="section-page-banner padding-y-128 bg-primary margin-top-116" id="section-page-banner">
  <div class="container">
    <div class="col-md-12 d-flex flex-wrap justify-content-between align-items-center">
      <div class="col-md-5">
        <div class="page-banner-header">
          <h1 class="font-40 font-bold text-white">
            Program
          </h1>
        </div>
      </div>
      <div class="col-md-6">
        <div class="page-banner-breadcrumb d-flex justify-content-end">
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item text-white opacity-50"><a href="#" class="text-decoration-none text-white">Home</a></li>
              <li class="breadcrumb-item text-white opacity-50"><a href="#" class="text-decoration-none text-white">Program</a></li>
              <li class="breadcrumb-item active text-white" aria-current="page">Kajian dan Pendampingan Perempuan</li>
            </ol>
          </nav>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="section-page-program padding-y-148 bg-light" id="section-page-program">
  <div class="container">
    <div class="row margin-x-48-negative flex-row-reverse">
      <div class="col-md-6 padding-x-48 padding-bottom-md-30">
        <div class="page-about-image">
          <img src="<?= base_url() ?>/assets/img/sections/prog-1.jpg" alt="" class="img-fluid padding-left-48 w-100">
          <img src="<?= base_url() ?>/assets/img/sections/about-1-before.jpg" alt="" class="img-fluid w-100 padding-right-48 margin-top-48">
        </div>
      </div>
      <div class="col-md-6 padding-x-48">
        <div class="page-about-content">
          <div class="about-content-header">
            <h5 class="font-40 font-bold mb-0"><span class="text-secondary">Program</span> Kajian & <br>Pendampingan Perempuan</h5>
          </div>
          <div class="about-content-body margin-y-36">
            <p class="font-roboto font-18 line-height-33 font-regular">
              Bidang Kajian dan Pendampingan Perempuan terdiri dari Koordinator Bidang Kajian Perempuan dan Koordinator Bidang Pendampingan Perempuan.
              <br><br>
              Kegiatan utama bidang Kajian Perempuan adalah melakukan kajian atas isu dan kondisi eksisting terkait perempuan termasuk mencari best practice yang ada di lingkungan BUMN dan lainnya untuk mendukung kebijakan bidang perempuan
              <br><br>
              Kegiatan utama bidang Pendampiangan Perempuan adalah untuk mendukung isu kesetaraan gender dan hak perempuan yang terjadi di tempat kerja guna merespon bias gender dimana perempuan dianggap tidak mampu atau tidak pantas melakukan pekerjaan tertentu, selain tentang pelecehan kepada perempuan (Quid Pro Quo Harassment atau Hostile Work Environment)
            </p>
          </div>
        </div>
      </div>
    </div>
    <div class="pengurus-content margin-top-248 margin-top-md-120">
      <div class="pengurus-header text-center">
        <h5 class="font-40 font-bold">Pengurus Srikandi BUMN<br>Kajian & Pendampingan Perempuan</h5>
        <p class="font-18 font-roboto font-regular margin-top-18">Pengukuhan Ketua Srikandi BUMN, Ibu Tina Kemala Intan dilakukan bersamaan dengan Ketua Umum FHCI dan Pengurus FHCI periode 2021-2024, yaitu pada 7 April 2021 oleh Menteri BUMN, Erick Thohir.</p>
      </div>
      <div class="pengurus-body margin-top-80">
        <div class="srikandi-team-body">
          <div class="row margin-x-24-negative">
            <div class="col-xl-3 col-lg-4 col-md-6 padding-x-24 mx-auto">
              <div class="srikandi-team-item margin-bottom-48 h-100">
                <div class="srikandi-team-image-wrapper">
                  <div class="team-image text-center">
                    <img src="<?= base_url() ?>/assets/img/sections/teams/1.jpg" alt="" class="img-fluid">
                  </div>
                  <div class="team-description padding-x-24 padding-y-40 bg-primary h-100 text-white d-flex flex-column justify-content-between">
                    <p class="font-roboto font-16 font-regular mb-0">
                      Tugas dan tanggungjawab Ketua Srikandi BUMN adalah mengkoordinasikan dan mengorganisasikan seluruh penyelenggaraan organisasi dan program kerjanya serta mempertanggungjawabkan secara internal kepada Pengurus Srikandi serta kepada induk organisasi yaitu Forum Human Capital Indonesia (FHCI).
                    </p>
                    <div class="d-flex justify-content-between align-items-center">
                      <a href="<?= base_url() ?>/tentang-kami/founder-dan-pengurus/..." class="btn btn-outline-light rounded-pill padding-y-8 font-regular padding-x-18 font-12">
                        <i class="bi bi-eye me-2"></i>
                        Profil Lengkap
                      </a>
                      <a href="javascript:;" class="d-block text-white font-22">
                        <i class="bi bi-linkedin"></i>
                      </a>
                    </div>
                  </div>
                </div>
                <div class="srikandi-team-content text-center">
                  <h6 class="font-20 font-bold margin-top-24 mb-0">Tina T. Ke mala Intan</h6>
                  <p class="text-transform-uppercase margin-y-8">Ketua</p>
                  <small class="opacity-50">Direktur SDM dan Hukum PT Semen Indonesia (Persero)</small>
                </div>
              </div>
            </div>
          </div>
          <div class="row margin-x-24-negative">
            <div class="col-xl-3 col-lg-4 col-md-6 padding-x-24">
              <div class="srikandi-team-item margin-bottom-48 h-100">
                <div class="srikandi-team-image-wrapper">
                  <div class="team-image text-center">
                    <img src="<?= base_url() ?>/assets/img/sections/teams/2.jpg" alt="" class="img-fluid">
                  </div>
                  <div class="team-description padding-x-24 padding-y-40 bg-primary h-100 text-white d-flex flex-column justify-content-between">
                    <p class="font-roboto font-16 font-regular mb-0">
                      Tugas dan tanggungjawab Ketua Srikandi BUMN adalah mengkoordinasikan dan mengorganisasikan seluruh penyelenggaraan organisasi dan program kerjanya serta mempertanggungjawabkan secara internal kepada Pengurus Srikandi serta kepada induk organisasi yaitu Forum Human Capital Indonesia (FHCI).
                    </p>
                    <div class="d-flex justify-content-between align-items-center">
                      <a href="<?= base_url() ?>/tentang-kami/founder-dan-pengurus/..." class="btn btn-outline-light rounded-pill padding-y-8 font-regular padding-x-18 font-12">
                        <i class="bi bi-eye me-2"></i>
                        Profil Lengkap
                      </a>
                      <a href="javascript:;" class="d-block text-white font-22">
                        <i class="bi bi-linkedin"></i>
                      </a>
                    </div>
                  </div>
                </div>
                <div class="srikandi-team-content text-center">
                  <h6 class="font-20 font-bold margin-top-24 mb-0">Judith J. Dipodiputro</h6>
                  <p class="text-transform-uppercase margin-y-8">Sekretaris Jenderal</p>
                  <small class="opacity-50">Direktur Utama Perum Produksi Film Negara</small>
                </div>
              </div>
            </div>
            <div class="col-xl-3 col-lg-4 col-md-6 padding-x-24">
              <div class="srikandi-team-item margin-bottom-48 h-100">
                <div class="srikandi-team-image-wrapper">
                  <div class="team-image text-center">
                    <img src="<?= base_url() ?>/assets/img/sections/teams/3.jpg" alt="" class="img-fluid">
                  </div>
                  <div class="team-description padding-x-24 padding-y-40 bg-primary h-100 text-white d-flex flex-column justify-content-between">
                    <p class="font-roboto font-16 font-regular mb-0">
                      Tugas dan tanggungjawab Ketua Srikandi BUMN adalah mengkoordinasikan dan mengorganisasikan seluruh penyelenggaraan organisasi dan program kerjanya serta mempertanggungjawabkan secara internal kepada Pengurus Srikandi serta kepada induk organisasi yaitu Forum Human Capital Indonesia (FHCI).
                    </p>
                    <div class="d-flex justify-content-between align-items-center">
                      <a href="<?= base_url() ?>/tentang-kami/founder-dan-pengurus/..." class="btn btn-outline-light rounded-pill padding-y-8 font-regular padding-x-18 font-12">
                        <i class="bi bi-eye me-2"></i>
                        Profil Lengkap
                      </a>
                      <a href="javascript:;" class="d-block text-white font-22">
                        <i class="bi bi-linkedin"></i>
                      </a>
                    </div>
                  </div>
                </div>
                <div class="srikandi-team-content text-center">
                  <h6 class="font-20 font-bold margin-top-24 mb-0">Dewi Aryani Suzana</h6>
                  <p class="text-transform-uppercase margin-y-8">Wakil Sekretaris Jenderal</p>
                  <small class="opacity-50">Direktur SDM & Umum Jasa Raharja</small>
                </div>
              </div>
            </div>
            <div class="col-xl-3 col-lg-4 col-md-6 padding-x-24">
              <div class="srikandi-team-item margin-bottom-48 h-100">
                <div class="srikandi-team-image-wrapper">
                  <div class="team-image text-center">
                    <img src="<?= base_url() ?>/assets/img/sections/teams/4.jpg" alt="" class="img-fluid">
                  </div>
                  <div class="team-description padding-x-24 padding-y-40 bg-primary h-100 text-white d-flex flex-column justify-content-between">
                    <p class="font-roboto font-16 font-regular mb-0">
                      Tugas dan tanggungjawab Ketua Srikandi BUMN adalah mengkoordinasikan dan mengorganisasikan seluruh penyelenggaraan organisasi dan program kerjanya serta mempertanggungjawabkan secara internal kepada Pengurus Srikandi serta kepada induk organisasi yaitu Forum Human Capital Indonesia (FHCI).
                    </p>
                    <div class="d-flex justify-content-between align-items-center">
                      <a href="<?= base_url() ?>/tentang-kami/founder-dan-pengurus/..." class="btn btn-outline-light rounded-pill padding-y-8 font-regular padding-x-18 font-12">
                        <i class="bi bi-eye me-2"></i>
                        Profil Lengkap
                      </a>
                      <a href="javascript:;" class="d-block text-white font-22">
                        <i class="bi bi-linkedin"></i>
                      </a>
                    </div>
                  </div>
                </div>
                <div class="srikandi-team-content text-center">
                  <h6 class="font-20 font-bold margin-top-24 mb-0">Winarsih Budiriani</h6>
                  <p class="text-transform-uppercase margin-y-8">Bendahara</p>
                  <small class="opacity-50">Direktur Keuangan dan Manajemen Risiko Perum Percetakan Uang Republik Indonesia (PERURI)</small>
                </div>
              </div>
            </div>
            <div class="col-xl-3 col-lg-4 col-md-6 padding-x-24">
              <div class="srikandi-team-item margin-bottom-48 h-100">
                <div class="srikandi-team-image-wrapper">
                  <div class="team-image text-center">
                    <img src="<?= base_url() ?>/assets/img/sections/teams/5.jpg" alt="" class="img-fluid">
                  </div>
                  <div class="team-description padding-x-24 padding-y-40 bg-primary h-100 text-white d-flex flex-column justify-content-between">
                    <p class="font-roboto font-16 font-regular mb-0">
                      Tugas dan tanggungjawab Ketua Srikandi BUMN adalah mengkoordinasikan dan mengorganisasikan seluruh penyelenggaraan organisasi dan program kerjanya serta mempertanggungjawabkan secara internal kepada Pengurus Srikandi serta kepada induk organisasi yaitu Forum Human Capital Indonesia (FHCI).
                    </p>
                    <div class="d-flex justify-content-between align-items-center">
                      <a href="<?= base_url() ?>/tentang-kami/founder-dan-pengurus/..." class="btn btn-outline-light rounded-pill padding-y-8 font-regular padding-x-18 font-12">
                        <i class="bi bi-eye me-2"></i>
                        Profil Lengkap
                      </a>
                      <a href="javascript:;" class="d-block text-white font-22">
                        <i class="bi bi-linkedin"></i>
                      </a>
                    </div>
                  </div>
                </div>
                <div class="srikandi-team-content text-center">
                  <h6 class="font-20 font-bold margin-top-24 mb-0">Merlany Legitasari</h6>
                  <p class="text-transform-uppercase margin-y-8">Wakil Bendahara</p>
                  <small class="opacity-50">Direktur PT Virama Karya (Persero)</small>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?= $this->endSection() ?>

<?= $this->section('prepend-script') ?>
<?= $this->endSection() ?>

<?= $this->section('append-script') ?>
<?= $this->endSection() ?>