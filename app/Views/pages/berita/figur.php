<?= $this->extend('layouts/base') ?>

<?= $this->section('prepend-style') ?>
<?= $this->endSection() ?>

<?= $this->section('append-style') ?>
<link rel="stylesheet" href="<?= base_url() ?>/assets/css/pages/berita-responsive.css">
<link rel="stylesheet" href="<?= base_url() ?>/assets/css/pages/berita.css">
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<section class="section-page-banner padding-y-128 bg-primary margin-top-116" id="section-page-banner">
  <div class="container">
    <div class="col-md-12 d-flex flex-wrap justify-content-between align-items-center">
      <div class="col-md-5">
        <div class="page-banner-header">
          <h1 class="font-40 font-bold text-white">
            Figur
          </h1>
        </div>
      </div>
      <div class="col-md-7">
        <div class="page-banner-breadcrumb d-flex justify-content-end">
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item text-white opacity-50"><a href="#" class="text-decoration-none text-white">Home</a></li>
              <li class="breadcrumb-item text-white opacity-50"><a href="#" class="text-decoration-none text-white">Berita</a></li>
              <li class="breadcrumb-item active text-white" aria-current="page">Figur</li>
            </ol>
          </nav>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="section-page-pers min-height-500 padding-y-100" id="section-page-pers">
  <div class="pers-wrapper">
    <div class="container">
      <div class="col-md-10 mx-auto">
        <div class="row margin-x-12-negative">
          <div class="col-xl-3 col-lg-4 col-md-5 padding-x-24">
            <div class="pers-sidebar">
              <form action="#" method="POST" class="pers-search">
                <div class="form-group border d-flex align-items-center">
                  <input type="text" class="form-control rounded-0 font-roboto border-0 font-15 padding-y-14 padding-x-18" placeholder="Cari Siaran Pers">
                  <img src="<?= base_url() ?>/assets/img/misc/search-icon.svg" alt="" width="18" class="margin-x-16">
                </div>
              </form>
              <div class="pers-category margin-top-58">
                <div class="category-header text-transform-uppercase">
                  <div class="bg-white padding-right-24 d-inline font-robot font-20 font-regular">Kategori</div>
                </div>
                <div class="category-body margin-top-16">
                  <ul class="list-unstyled">
                    <li><a href="#" class="font-18 font-roboto font-regular btn btn-link px-0 text-dark opacity-50 text-decoration-none padding-y-12 border-bottom w-100 text-start">Arts and Entertainment</a></li>
                    <li><a href="#" class="font-18 font-roboto font-regular btn btn-link px-0 text-dark opacity-50 text-decoration-none padding-y-12 border-bottom w-100 text-start">Blog</a></li>
                    <li><a href="#" class="font-18 font-roboto font-regular btn btn-link px-0 text-dark opacity-50 text-decoration-none padding-y-12 border-bottom w-100 text-start">Blog Full width</a></li>
                    <li><a href="#" class="font-18 font-roboto font-regular btn btn-link px-0 text-dark opacity-50 text-decoration-none padding-y-12 border-bottom w-100 text-start">Blog Grid</a></li>
                    <li><a href="#" class="font-18 font-roboto font-regular btn btn-link px-0 text-dark opacity-50 text-decoration-none padding-y-12 border-bottom w-100 text-start">Branding</a></li>
                    <li><a href="#" class="font-18 font-roboto font-regular btn btn-link px-0 text-dark opacity-50 text-decoration-none padding-y-12 border-bottom w-100 text-start">Design Tutorials</a></li>
                    <li><a href="#" class="font-18 font-roboto font-regular btn btn-link px-0 text-dark opacity-50 text-decoration-none padding-y-12 border-bottom w-100 text-start">Designing</a></li>
                    <li><a href="#" class="font-18 font-roboto font-regular btn btn-link px-0 text-dark opacity-50 text-decoration-none padding-y-12 border-bottom w-100 text-start">Feature</a></li>
                    <li><a href="#" class="font-18 font-roboto font-regular btn btn-link px-0 text-dark opacity-50 text-decoration-none padding-y-12 border-bottom w-100 text-start">Home Blog</a></li>
                    <li><a href="#" class="font-18 font-roboto font-regular btn btn-link px-0 text-dark opacity-50 text-decoration-none padding-y-12 border-bottom w-100 text-start">Onepage Fashion</a></li>
                    <li><a href="#" class="font-18 font-roboto font-regular btn btn-link px-0 text-dark opacity-50 text-decoration-none padding-y-12 w-100 text-start">Sample</a></li>
                  </ul>
                </div>
              </div>
              <div class="pers-other margin-top-58">
                <div class="other-header text-transform-uppercase">
                  <div class="bg-white padding-right-24 d-inline font-robot font-20 font-regular">Siaran pers lain</div>
                </div>
                <div class="other-body margin-top-16">
                  <div class="other-item mb-2">
                    <div class="row m-0">
                      <div class="col-4 p-0">
                        <div class="h-100 overflow-hidden border min-height-64 max-height-80">
                          <img src="<?= base_url() ?>/assets/img/blogs/pers/Image 52.jpg" alt="" class="w-100 h-100 object-fit-cover">
                        </div>
                      </div>
                      <div class="col-8 ps-3 py-2">
                        <h6 class="font-bold font-roboto font-14">Lorem ipsum dolor sit amet.</h6>
                        <p class="font-light font-14 font-roboto">30 Juni 2020</p>
                      </div>
                    </div>
                  </div>
                  <div class="other-item mb-2">
                    <div class="row m-0">
                      <div class="col-4 p-0">
                        <div class="h-100 overflow-hidden border min-height-64 max-height-80">
                          <img src="<?= base_url() ?>/assets/img/blogs/pers/Image 52.jpg" alt="" class="w-100 h-100 object-fit-cover">
                        </div>
                      </div>
                      <div class="col-8 ps-3 py-2">
                        <h6 class="font-bold font-roboto font-14">Lorem ipsum dolor sit amet.</h6>
                        <p class="font-light font-14 font-roboto">30 Juni 2020</p>
                      </div>
                    </div>
                  </div>
                  <div class="other-item mb-2">
                    <div class="row m-0">
                      <div class="col-4 p-0">
                        <div class="h-100 overflow-hidden border min-height-64 max-height-80">
                          <img src="<?= base_url() ?>/assets/img/blogs/pers/Image 52.jpg" alt="" class="w-100 h-100 object-fit-cover">
                        </div>
                      </div>
                      <div class="col-8 ps-3 py-2">
                        <h6 class="font-bold font-roboto font-14">Lorem ipsum dolor sit amet.</h6>
                        <p class="font-light font-14 font-roboto">30 Juni 2020</p>
                      </div>
                    </div>
                  </div>
                  <div class="other-item mb-2">
                    <div class="row m-0">
                      <div class="col-4 p-0">
                        <div class="h-100 overflow-hidden border min-height-64 max-height-80">
                          <img src="<?= base_url() ?>/assets/img/blogs/pers/Image 52.jpg" alt="" class="w-100 h-100 object-fit-cover">
                        </div>
                      </div>
                      <div class="col-8 ps-3 py-2">
                        <h6 class="font-bold font-roboto font-14">Lorem ipsum dolor sit amet.</h6>
                        <p class="font-light font-14 font-roboto">30 Juni 2020</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-xl-9 col-lg-8 col-md-7 padding-x-24 padding-top-md-80">
            <div class="pers-content">
              <div class="content-wrapper border margin-bottom-86">
                <div class="content-image max-height-580 overflow-hidden">
                  <img src="<?= base_url() ?>/assets/img/blogs/pers/Image 52.jpg" alt="" class="w-100 h-100 object-fit-cover object-position-center">
                </div>
                <div class="content-description">
                  <div class="description-header padding-x-50 padding-top-60">
                    <div class="date">
                      <span class="font-12 font-regular opacity-50">Terbit pada 30 juni 2020</span>
                    </div>
                    <div class="title margin-top-8">
                      <h5 class="font-bold font-27 font-roboto">Pentingnya Sinergi Antara Senior dan Generasi Muda Untuk Memenangkan Persaingan Masa Depan</h5>
                    </div>
                  </div>
                  <div class="description-body padding-x-50 padding-top-28">
                    <p class="mb-0 font-20 font-regular opacity-50">Senin, 14 Juni 2021 – Kementerian BUMN berkolaborasi dengan Forum Human Capital Indonesia (FHCI) dan BUMN Muda menyelenggarakan talkshow Erick Thohir Menyapa: Fast Break Menuju Generasi Emas BUMN untuk membangkitkan semangat dan motivasi para talenta muda BUMN agar menjadi </p>
                  </div>
                  <div class="description-footer padding-top-48">
                    <div class="col-md-12 d-flex m-0 justify-content-between">
                      <div class="col-md-4 footer-profile border-top">
                        <a class="d-flex flex-wrap align-items-center h-100 text-decoration-none text-dark" href="javascript:;">
                          <img src="<?= base_url() ?>/assets/img/misc/user.jpg" alt="" width="44" class="margin-right-8">
                          <span class="opacity-50 font-18 font-light font-roboto">Adriansyah</span>
                        </a>
                      </div>
                      <div class="col-md-4 footer-likes border-top border-start border-end">
                        <a class="d-flex flex-wrap align-items-center h-100 text-decoration-none text-dark" href="javascript:;">
                          <i class="bi bi-heart margin-right-18 opacity-50"></i>
                          <span class="opacity-50 font-18 font-light font-roboto">15 Like(s)</span>
                        </a>
                      </div>
                      <div class="col-md-4 footer-comments border-top">
                        <a class="d-flex flex-wrap align-items-center h-100 text-decoration-none text-dark" href="javascript:;">
                          <img src="<?= base_url() ?>/assets/img/misc/comment-icon.svg" alt="" width="16" class="margin-right-8 opacity-50">
                          <span class="opacity-50 font-18 font-light font-roboto">3 Comment(s)</span>
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="content-wrapper border margin-bottom-86">
                <div class="content-image max-height-580 overflow-hidden">
                  <img src="<?= base_url() ?>/assets/img/blogs/pers/Image 64.jpg" alt="" class="w-100 h-100 object-fit-cover object-position-center">
                </div>
                <div class="content-description">
                  <div class="description-header padding-x-50 padding-top-60">
                    <div class="date">
                      <span class="font-12 font-regular opacity-50">Terbit pada 30 juni 2020</span>
                    </div>
                    <div class="title margin-top-8">
                      <h5 class="font-bold font-27 font-roboto">Pentingnya Sinergi Antara Senior dan Generasi Muda Untuk Memenangkan Persaingan Masa Depan</h5>
                    </div>
                  </div>
                  <div class="description-body padding-x-50 padding-top-28">
                    <p class="mb-0 font-20 font-regular opacity-50">Senin, 14 Juni 2021 – Kementerian BUMN berkolaborasi dengan Forum Human Capital Indonesia (FHCI) dan BUMN Muda menyelenggarakan talkshow Erick Thohir Menyapa: Fast Break Menuju Generasi Emas BUMN untuk membangkitkan semangat dan motivasi para talenta muda BUMN agar menjadi </p>
                  </div>
                  <div class="description-footer padding-top-48">
                    <div class="col-md-12 d-flex m-0 justify-content-between">
                      <div class="col-md-4 footer-profile border-top">
                        <a class="d-flex flex-wrap align-items-center h-100 text-decoration-none text-dark" href="javascript:;">
                          <img src="<?= base_url() ?>/assets/img/misc/user.jpg" alt="" width="44" class="margin-right-8">
                          <span class="opacity-50 font-18 font-light font-roboto">Adriansyah</span>
                        </a>
                      </div>
                      <div class="col-md-4 footer-likes border-top border-start border-end">
                        <a class="d-flex flex-wrap align-items-center h-100 text-decoration-none text-dark" href="javascript:;">
                          <i class="bi bi-heart margin-right-18 opacity-50"></i>
                          <span class="opacity-50 font-18 font-light font-roboto">15 Like(s)</span>
                        </a>
                      </div>
                      <div class="col-md-4 footer-comments border-top">
                        <a class="d-flex flex-wrap align-items-center h-100 text-decoration-none text-dark" href="javascript:;">
                          <img src="<?= base_url() ?>/assets/img/misc/comment-icon.svg" alt="" width="16" class="margin-right-8 opacity-50">
                          <span class="opacity-50 font-18 font-light font-roboto">3 Comment(s)</span>
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="content-wrapper border margin-bottom-86">
                <div class="content-image max-height-580 overflow-hidden">
                  <img src="<?= base_url() ?>/assets/img/blogs/pers/Image 65.jpg" alt="" class="w-100 h-100 object-fit-cover object-position-center">
                </div>
                <div class="content-description">
                  <div class="description-header padding-x-50 padding-top-60">
                    <div class="date">
                      <span class="font-12 font-regular opacity-50">Terbit pada 30 juni 2020</span>
                    </div>
                    <div class="title margin-top-8">
                      <h5 class="font-bold font-27 font-roboto">Pentingnya Sinergi Antara Senior dan Generasi Muda Untuk Memenangkan Persaingan Masa Depan</h5>
                    </div>
                  </div>
                  <div class="description-body padding-x-50 padding-top-28">
                    <p class="mb-0 font-20 font-regular opacity-50">Senin, 14 Juni 2021 – Kementerian BUMN berkolaborasi dengan Forum Human Capital Indonesia (FHCI) dan BUMN Muda menyelenggarakan talkshow Erick Thohir Menyapa: Fast Break Menuju Generasi Emas BUMN untuk membangkitkan semangat dan motivasi para talenta muda BUMN agar menjadi </p>
                  </div>
                  <div class="description-footer padding-top-48">
                    <div class="col-md-12 d-flex m-0 justify-content-between">
                      <div class="col-md-4 footer-profile border-top">
                        <a class="d-flex flex-wrap align-items-center h-100 text-decoration-none text-dark" href="javascript:;">
                          <img src="<?= base_url() ?>/assets/img/misc/user.jpg" alt="" width="44" class="margin-right-8">
                          <span class="opacity-50 font-18 font-light font-roboto">Adriansyah</span>
                        </a>
                      </div>
                      <div class="col-md-4 footer-likes border-top border-start border-end">
                        <a class="d-flex flex-wrap align-items-center h-100 text-decoration-none text-dark" href="javascript:;">
                          <i class="bi bi-heart margin-right-18 opacity-50"></i>
                          <span class="opacity-50 font-18 font-light font-roboto">15 Like(s)</span>
                        </a>
                      </div>
                      <div class="col-md-4 footer-comments border-top">
                        <a class="d-flex flex-wrap align-items-center h-100 text-decoration-none text-dark" href="javascript:;">
                          <img src="<?= base_url() ?>/assets/img/misc/comment-icon.svg" alt="" width="16" class="margin-right-8 opacity-50">
                          <span class="opacity-50 font-18 font-light font-roboto">3 Comment(s)</span>
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?= $this->endSection() ?>

<?= $this->section('prepend-script') ?>
<?= $this->endSection() ?>

<?= $this->section('append-script') ?>
<?= $this->endSection() ?>