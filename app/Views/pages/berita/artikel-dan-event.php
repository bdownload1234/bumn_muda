<?= $this->extend('layouts/base') ?>

<?= $this->section('prepend-style') ?>
<?= $this->endSection() ?>

<?= $this->section('append-style') ?>
<link rel="stylesheet" href="<?= base_url() ?>/assets/css/pages/ae-responsive.css">
<link rel="stylesheet" href="<?= base_url() ?>/assets/css/pages/artikel-dan-event.css">
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<section class="section-page-banner padding-y-128 bg-primary margin-top-116" id="section-page-banner">
  <div class="container">
    <div class="col-md-12 d-flex flex-wrap justify-content-between align-items-center">
      <div class="col-md-5">
        <div class="page-banner-header">
          <h1 class="font-40 font-bold text-white">
            Artikel Srikandi
          </h1>
        </div>
      </div>
      <div class="col-md-7">
        <div class="page-banner-breadcrumb d-flex justify-content-end">
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item text-white opacity-50"><a href="#" class="text-decoration-none text-white">Home</a></li>
              <li class="breadcrumb-item text-white opacity-50"><a href="#" class="text-decoration-none text-white">Berita</a></li>
              <li class="breadcrumb-item active text-white" aria-current="page">Artikel dan Event</li>
            </ol>
          </nav>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="section min-height-450 padding-y-120" id="section-blog">
  <div class="blog-wrapper">
    <div class="container">
      <div class="blog-wrapper-container grid" data-masonry='{ "itemSelector": ".blog-item" }'>
        <div class="blog-item grid-item">
          <div class="blog-item-wrapper bg-light">
            <div class="blog-header">
              <div class="blog-image">
                <img src="<?= base_url() ?>/assets/img/blogs/one.jpg" alt="" class="img-fluid w-100">
              </div>
            </div>
            <div class="blog-body padding-x-22">
              <div class="blog-title padding-top-22">
                <a href="#" class="btn-link text-decoration-none text-dark">
                  <h6 class="font-18 font-roboto font-medium">The 3rd Indonesia Human Capital Summit 2021...</h6>
                </a>
              </div>
              <div class="blog-meta margin-top-12 border-bottom">
                <p class="font-15 font-roboto font-regular line-height-20 opacity-50">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Eveniet, excepturi.</p>
              </div>
            </div>
            <div class="blog-footer padding-x-22 padding-y-16 padding-bottom-22">
              <span class="font-12 font-roboto font-regular text-transform-uppercase opacity-50">
                18 November 2021
              </span>
            </div>
          </div>
        </div>
        <div class="blog-item grid-item">
          <div class="blog-item-wrapper bg-light">
            <div class="blog-header">
              <div class="blog-image">
                <img src="<?= base_url() ?>/assets/img/blogs/two.jpg" alt="" class="img-fluid w-100">
              </div>
            </div>
            <div class="blog-body padding-x-22">
              <div class="blog-title padding-top-22">
                <a href="#" class="btn-link text-decoration-none text-dark">
                  <h6 class="font-18 font-roboto font-medium">The 3rd Indonesia Human Capital Summit 2021...</h6>
                </a>
              </div>
              <div class="blog-meta margin-top-12 border-bottom">
                <p class="font-15 font-roboto font-regular line-height-20 opacity-50">Lorem ipsum dolor sit amet consectetur adipisicing elit. Delectus, fugiat? Minus dolores fuga maiores quaerat ea eligendi facere nulla at?</p>
              </div>
            </div>
            <div class="blog-footer padding-x-22 padding-y-16 padding-bottom-22">
              <span class="font-12 font-roboto font-regular text-transform-uppercase opacity-50">
                18 November 2021
              </span>
            </div>
          </div>
        </div>
        <div class="blog-item grid-item">
          <div class="blog-item-wrapper bg-light">
            <div class="blog-header">
              <div class="blog-image">
                <img src="<?= base_url() ?>/assets/img/blogs/three.jpg" alt="" class="img-fluid w-100">
              </div>
            </div>
            <div class="blog-body padding-x-22">
              <div class="blog-title padding-top-22">
                <a href="#" class="btn-link text-decoration-none text-dark">
                  <h6 class="font-18 font-roboto font-medium">The 3rd Indonesia Human Capital Summit 2021...</h6>
                </a>
              </div>
              <div class="blog-meta margin-top-12 border-bottom">
                <p class="font-15 font-roboto font-regular line-height-20 opacity-50">The 3rd IHCS 2021 kali ini menghadirkan parapakar Human Capital management dari dalam dan luar negeri yang ...</p>
              </div>
            </div>
            <div class="blog-footer padding-x-22 padding-y-16 padding-bottom-22">
              <span class="font-12 font-roboto font-regular text-transform-uppercase opacity-50">
                18 November 2021
              </span>
            </div>
          </div>
        </div>
        <div class="blog-item grid-item">
          <div class="blog-item-wrapper bg-light">
            <div class="blog-header">
              <div class="blog-image">
                <img src="<?= base_url() ?>/assets/img/blogs/three.jpg" alt="" class="img-fluid w-100">
              </div>
            </div>
            <div class="blog-body padding-x-22">
              <div class="blog-title padding-top-22">
                <a href="#" class="btn-link text-decoration-none text-dark">
                  <h6 class="font-18 font-roboto font-medium">The 3rd Indonesia Human Capital Summit 2021...</h6>
                </a>
              </div>
              <div class="blog-meta margin-top-12 border-bottom">
                <p class="font-15 font-roboto font-regular line-height-20 opacity-50">The 3rd IHCS 2021 kali ini menghadirkan parapakar Human Capital management dari dalam dan luar negeri yang ...</p>
              </div>
            </div>
            <div class="blog-footer padding-x-22 padding-y-16 padding-bottom-22">
              <span class="font-12 font-roboto font-regular text-transform-uppercase opacity-50">
                18 November 2021
              </span>
            </div>
          </div>
        </div>
        <div class="blog-item grid-item">
          <div class="blog-item-wrapper bg-light">
            <div class="blog-header">
              <div class="blog-image">
                <img src="<?= base_url() ?>/assets/img/blogs/three.jpg" alt="" class="img-fluid w-100">
              </div>
            </div>
            <div class="blog-body padding-x-22">
              <div class="blog-title padding-top-22">
                <a href="#" class="btn-link text-decoration-none text-dark">
                  <h6 class="font-18 font-roboto font-medium">The 3rd Indonesia Human Capital Summit 2021...</h6>
                </a>
              </div>
              <div class="blog-meta margin-top-12 border-bottom">
                <p class="font-15 font-roboto font-regular line-height-20 opacity-50">The 3rd IHCS 2021 kali ini menghadirkan parapakar Human Capital management dari dalam dan luar negeri yang ...</p>
              </div>
            </div>
            <div class="blog-footer padding-x-22 padding-y-16 padding-bottom-22">
              <span class="font-12 font-roboto font-regular text-transform-uppercase opacity-50">
                18 November 2021
              </span>
            </div>
          </div>
        </div>
        <div class="blog-item grid-item">
          <div class="blog-item-wrapper bg-light">
            <div class="blog-header">
              <div class="blog-image">
                <img src="<?= base_url() ?>/assets/img/blogs/one.jpg" alt="" class="img-fluid w-100">
              </div>
            </div>
            <div class="blog-body padding-x-22">
              <div class="blog-title padding-top-22">
                <a href="#" class="btn-link text-decoration-none text-dark">
                  <h6 class="font-18 font-roboto font-medium">The 3rd Indonesia Human Capital Summit 2021...</h6>
                </a>
              </div>
              <div class="blog-meta margin-top-12 border-bottom">
                <p class="font-15 font-roboto font-regular line-height-20 opacity-50">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Eveniet, excepturi.</p>
              </div>
            </div>
            <div class="blog-footer padding-x-22 padding-y-16 padding-bottom-22">
              <span class="font-12 font-roboto font-regular text-transform-uppercase opacity-50">
                18 November 2021
              </span>
            </div>
          </div>
        </div>
      </div>
      <div class="blog-pagination d-flex justify-content-center">
        <nav>
          <ul class="pagination">
            <li class="page-item disabled">
              <a class="page-link rounded-0"><i class="bi bi-arrow-left-short me-1"></i> Previous</a>
            </li>
            <li class="page-item"><a class="page-link" href="#">1</a></li>
            <li class="page-item active" aria-current="page">
              <a class="page-link" href="#">2</a>
            </li>
            <li class="page-item"><a class="page-link" href="#">3</a></li>
            <li class="page-item">
              <a class="page-link rounded-0" href="#">Next <i class="bi bi-arrow-right-short ms-1"></i></a>
            </li>
          </ul>
        </nav>
      </div>
    </div>
  </div>
</section>
<?= $this->endSection() ?>

<?= $this->section('prepend-script') ?>
<?= $this->endSection() ?>

<?= $this->section('append-script') ?>
<script src="https://unpkg.com/masonry-layout@4/dist/masonry.pkgd.js"></script>
<?= $this->endSection() ?>