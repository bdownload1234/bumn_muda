<?= $this->extend('layouts/base') ?>

<?= $this->section('prepend-style') ?>
<?= $this->endSection() ?>

<?= $this->section('append-style') ?>
<link rel="stylesheet" href="<?= base_url() ?>/assets/css/pages/gallery-responsive.css">
<link rel="stylesheet" href="<?= base_url() ?>/assets/css/pages/gallery.css">
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<section class="section-page-banner padding-y-128 bg-primary margin-top-126" id="section-page-banner">
  <div class="container">
    <div class="col-md-12 d-flex flex-wrap justify-content-between align-items-center">
      <div class="col-md-5">
        <div class="page-banner-header">
          <h1 class="font-40 font-bold text-white">
            Gallery Srikandi
          </h1>
        </div>
      </div>
      <div class="col-md-7">
        <div class="page-banner-breadcrumb d-flex justify-content-end">
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item text-white opacity-50"><a href="#" class="text-decoration-none text-white">Home</a></li>
              <li class="breadcrumb-item active text-white" aria-current="page">Gallery Srikandi</li>
            </ol>
          </nav>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="section min-height-450 padding-y-120" id="section-gallery">
  <div class="gallery-wrapper">
    <div class="container">
      <div class="gallery-container grid" data-masonry='{ "itemSelector": ".gallery-item"}'>
        <div class="gallery-item grid-item">
          <div class="gallery-item-wrapper">
            <img src="assets/img/gallery/Component 31 – 2.jpg" alt="" class="img-fluid">
            <div class="gallery-description text-center bg-white padding-y-18 padding-x-24">
              <div class="description-title">
                <h6 class="font-roboto font-20 font-medium mb-0">Pentingnya Sinergi</h6>
              </div>
              <div class="description-date">
                <span class="font-roboto font-14 font-light">20 April 2020</span>
              </div>
            </div>
          </div>
        </div>
        <div class="gallery-item grid-item">
          <div class="gallery-item-wrapper">
            <img src="assets/img/gallery/Component 32 – 2.jpg" alt="" class="img-fluid">
            <div class="gallery-description text-center bg-white padding-y-18 padding-x-24">
              <div class="description-title">
                <h6 class="font-roboto font-20 font-medium mb-0">Pentingnya Sinergi</h6>
              </div>
              <div class="description-date">
                <span class="font-roboto font-14 font-light">20 April 2020</span>
              </div>
            </div>
          </div>
        </div>
        <div class="gallery-item grid-item">
          <div class="gallery-item-wrapper">
            <img src="assets/img/gallery/Component 33 – 1.jpg" alt="" class="img-fluid">
            <div class="gallery-description text-center bg-white padding-y-18 padding-x-24">
              <div class="description-title">
                <h6 class="font-roboto font-20 font-medium mb-0">Pentingnya Sinergi</h6>
              </div>
              <div class="description-date">
                <span class="font-roboto font-14 font-light">20 April 2020</span>
              </div>
            </div>
          </div>
        </div>
        <div class="gallery-item grid-item">
          <div class="gallery-item-wrapper">
            <img src="assets/img/gallery/Component 34 – 1.jpg" alt="" class="img-fluid">
            <div class="gallery-description text-center bg-white padding-y-18 padding-x-24">
              <div class="description-title">
                <h6 class="font-roboto font-20 font-medium mb-0">Pentingnya Sinergi</h6>
              </div>
              <div class="description-date">
                <span class="font-roboto font-14 font-light">20 April 2020</span>
              </div>
            </div>
          </div>
        </div>
        <div class="gallery-item grid-item">
          <div class="gallery-item-wrapper">
            <img src="assets/img/gallery/Component 35 – 1.jpg" alt="" class="img-fluid">
            <div class="gallery-description text-center bg-white padding-y-18 padding-x-24">
              <div class="description-title">
                <h6 class="font-roboto font-20 font-medium mb-0">Pentingnya Sinergi</h6>
              </div>
              <div class="description-date">
                <span class="font-roboto font-14 font-light">20 April 2020</span>
              </div>
            </div>
          </div>
        </div>
        <div class="gallery-item grid-item">
          <div class="gallery-item-wrapper">
            <img src="assets/img/gallery/Component 36 – 1.jpg" alt="" class="img-fluid">
            <div class="gallery-description text-center bg-white padding-y-18 padding-x-24">
              <div class="description-title">
                <h6 class="font-roboto font-20 font-medium mb-0">Pentingnya Sinergi</h6>
              </div>
              <div class="description-date">
                <span class="font-roboto font-14 font-light">20 April 2020</span>
              </div>
            </div>
          </div>
        </div>
        <div class="gallery-item grid-item">
          <div class="gallery-item-wrapper">
            <img src="assets/img/gallery/Component 38 – 1.jpg" alt="" class="img-fluid">
            <div class="gallery-description text-center bg-white padding-y-18 padding-x-24">
              <div class="description-title">
                <h6 class="font-roboto font-20 font-medium mb-0">Pentingnya Sinergi</h6>
              </div>
              <div class="description-date">
                <span class="font-roboto font-14 font-light">20 April 2020</span>
              </div>
            </div>
          </div>
        </div>
      </div>
      </div>
      <div class="gallery-pagination d-flex justify-content-center margin-top-96">
        <nav>
          <ul class="pagination">
            <li class="page-item disabled">
              <a class="page-link rounded-0"><i class="bi bi-arrow-left-short me-1"></i> Previous</a>
            </li>
            <li class="page-item"><a class="page-link" href="#">1</a></li>
            <li class="page-item active" aria-current="page">
              <a class="page-link" href="#">2</a>
            </li>
            <li class="page-item"><a class="page-link" href="#">3</a></li>
            <li class="page-item">
              <a class="page-link rounded-0" href="#">Next <i class="bi bi-arrow-right-short ms-1"></i></a>
            </li>
          </ul>
        </nav>
      </div>
    </div>
  </div>
</section>
<?= $this->endSection() ?>

<?= $this->section('prepend-script') ?>
<?= $this->endSection() ?>

<?= $this->section('append-script') ?>
<script src="https://unpkg.com/masonry-layout@4/dist/masonry.pkgd.js"></script>
<?= $this->endSection() ?>