<?= $this->extend('layouts/base') ?>

<?= $this->section('prepend-style') ?>
<?= $this->endSection() ?>

<?= $this->section('append-style') ?>
<link rel="stylesheet" href="<?= base_url() ?>/assets/css/pages/contact-responsive.css">
<link rel="stylesheet" href="<?= base_url() ?>/assets/css/pages/contact.css">
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<section class="section-page-banner padding-y-128 bg-primary margin-top-126" id="section-page-banner">
  <div class="container">
    <div class="col-md-12 d-flex flex-wrap justify-content-between align-items-center">
      <div class="col-md-5">
        <div class="page-banner-header">
          <h1 class="font-40 font-bold text-white">
            Kontak Kami
          </h1>
        </div>
      </div>
      <div class="col-md-7">
        <div class="page-banner-breadcrumb d-flex justify-content-end">
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item text-white opacity-50"><a href="#" class="text-decoration-none text-white">Home</a></li>
              <li class="breadcrumb-item active text-white" aria-current="page">Kontak Kami</li>
            </ol>
          </nav>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="section min-height-400 padding-y-100" id="section-contact-cta">
  <div class="contact-cta-wrapper">
    <div class="container">
      <div class="conatct-cta-header text-center">
        <h5 class="font-medium font-40">Kontak Kami</h5>
      </div>
      <div class="contact-cta-body margin-top-96">
        <div class="row">
          <div class="col-lg-4 col-md-6">
            <div class="contact-cta-inner text-center">
              <img src="assets/img/misc/location-icon.svg" alt="">
              <p class="mb-0 font-light font-roboto font-23 margin-top-32">Gd. Plaza Mandiri lt.28</p>
            </div>
          </div>
          <div class="col-lg-4 col-md-6">
            <div class="contact-cta-inner text-center">
              <img src="assets/img/misc/message-icon.svg" alt="">
              <p class="mb-0 font-light font-roboto font-23 margin-top-32">021 27095432</p>
            </div>
          </div>
          <div class="col-lg-4 col-md-6">
            <div class="contact-cta-inner text-center">
              <img src="assets/img/misc/email-icon.svg" alt="">
              <p class="mb-0 font-light font-roboto font-23 margin-top-32">fhcibumn@gmail.com</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="section min-height-400" id="section-contact-form">
  <div class="contact-form bg-primary">
    <div class="container container-no-xl">
      <div class="row m-0 align-items-center">
        <div class="col-md-6 p-0">
          <div class="contact-form-image">
            <img src="assets/img/sections/contact-form.jpg" alt="" class="h-100 w-100 object-fit-cover">
          </div>
        </div>
        <div class="col-md-6 p-0">
          <div class="contact-form-content h-100 padding-x-86 padding-y-86 text-white">
            <div class="contact-form-header">
              <h5 class="font-medium font-28">Tanyakan kami langsung</h5>
            </div>
            <form action="#" method="POST" class="margin-top-32">
              <div class="row margin-bottom-36">
                <div class="col-6">
                  <div class="form-group">
                    <input type="text" class="form-control bg-transparent rounded-0 border-0 border-bottom text-light placeholder-white font-regular padding-x-0 padding-y-18" placeholder="Nama">
                  </div>
                </div>
                <div class="col-6">
                  <div class="form-group">
                    <input type="text" class="form-control bg-transparent rounded-0 border-0 border-bottom text-light placeholder-white font-regular padding-x-0 padding-y-18" placeholder="Email">
                  </div>
                </div>
              </div>
              <div class="form-group">
                <textarea class="form-control bg-transparent rounded-0 border-0 border-bottom text-light placeholder-white font-regular padding-x-0 padding-y-18" rows="1">Pesan</textarea>
              </div>
              <button type="submit" class="btn btn-outline-light rounded-pill margin-top-76 font-16 text-transform-uppercase font-semi-bold padding-x-56 padding-y-12">
                Submit
              </button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section>
  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3966.29381276269!2d106.81231211516906!3d-6.224936995493854!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f5706bb18817%3A0xc2e3d9ddfde12334!2sForum%20Human%20Capital%20Indonesia!5e0!3m2!1sen!2sid!4v1655321258265!5m2!1sen!2sid" width="100%" height="700" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
</section>
<?= $this->endSection() ?>

<?= $this->section('prepend-script') ?>
<?= $this->endSection() ?>

<?= $this->section('append-script') ?>
<?= $this->endSection() ?>