<?= $this->extend('layouts/base') ?>

<?= $this->section('append-style') ?>
<link rel="stylesheet" href="<?= base_url() ?>/assets/css/pages/al-responsive.css">
<link rel="stylesheet" href="<?= base_url() ?>/assets/css/pages/arti-logo.css">
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<section class="section-page-banner padding-y-128 bg-primary margin-top-116" id="section-page-banner">
  <div class="container">
    <div class="col-md-12 d-flex flex-wrap justify-content-between align-items-center">
      <div class="col-md-5">
        <div class="page-banner-header">
          <h1 class="font-40 font-bold text-white">
            Tentang<br>Srikandi BUMN
          </h1>
        </div>
      </div>
      <div class="col-md-4">
        <div class="page-banner-breadcrumb d-flex justify-content-end">
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item text-white opacity-50"><a href="#" class="text-decoration-none text-white">Home</a></li>
              <li class="breadcrumb-item text-white opacity-50"><a href="#" class="text-decoration-none text-white">Tentang Kami</a></li>
              <li class="breadcrumb-item active text-white" aria-current="page">Arti dibalik Logo</li>
            </ol>
          </nav>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="section-page-logo-meaning padding-y-180" id="section-page-logo-meaning">
  <div class="logo-meaning-wrapper">
    <div class="container">
      <div class="logo-meaning-wrapper-inner">
        <div class="row align-items-center">
          <div class="col-md-6">
            <div class="logo-meaning-content">
              <div class="logo-meaning-header">
                <h5 class="font-40 font-bold">Arti dibalik Logo<br> Srikandi BUMN</h5>
              </div>
              <div class="logo-meaning-body font-roboto font-23 line-height-33 margin-top-48">
                <strong>
                  <p>
                    Logo menggambarkan seorang perempuan dengan sayap sebagaimana kupu-kupu. Hal ini menggambarkan tentang karakter utama kupu-kupu, yaitu:
                  </p>
                </strong>
                <p class="margin-top-36">
                  <span class="text-primary">Berubah Sempurna</span>, melalui metamorfosis sempurna mulai dari telur, telur menjadi ulat, ulat menjadi kepompong, kemudian setelah itu barulah menjadi kupu-kupu. Demikian pula diharapkan para perempuan BUMN melakukan proses perubahan diri menjadi menyempurna.
                  <br><br>
                  <span class="text-primary">Indah dan Bersih</span>, menyenangkan dipandang mata, serta hanya menyantap makanan yang bersih dan sehat.
                  <br><br>
                  <span class="text-primary">Memberikan Manfaat</span>, selalu menyerbuk tanaman yang membantu bunga-bunga berkembang menjadi buah. Bunga-bunga membuat alam menjadi indah. Buah-buahan juga bermanfaat untuk dimakan oleh manusia.
                </p>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="logo-meaning-media text-center">
              <img src="<?= base_url() ?>/assets/img/globals/srikandi-icon.svg" alt="" class="">
            </div>
          </div>
        </div>  
      </div>
      <div class="accordion margin-top-48" id="faqAccordion">
        <div class="accordion-item border-0 rounded-0 bg-transparent">
          <h2 class="accordion-header" id="one">
            <button class="accordion-button bg-transparent border-top border-bottom rounded-0 font-20 padding-y-24 text-transform-uppercase" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
              Arti Figur Perempuan
            </button>
          </h2>
          <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="one" data-bs-parent="#faqAccordion">
            <div class="accordion-body">
              <p class="font-20 font-roboto opacity-50">Lorem ipsum dolor sit amet consectetur adipisicing elit. Consequuntur aut necessitatibus eos aliquam esse numquam eveniet aspernatur laboriosam explicabo at dignissimos quos quis eius expedita dolorum harum architecto iure, tempora voluptatem delectus porro qui veritatis consectetur! Esse nihil illo animi facilis culpa, laborum at veniam fugiat, odio ab error quas non reprehenderit perferendis, labore assumenda perspiciatis. Nesciunt unde recusandae nisi harum facilis inventore, sit nihil vitae ipsa. Corrupti officia expedita facere incidunt officiis, a sunt. Fugit nostrum autem iure. Cupiditate obcaecati magni officia dolorem voluptatum corrupti beatae. Facere mollitia iusto ut recusandae possimus excepturi, esse nulla ipsam, ullam vel saepe?</p>
            </div>
          </div>
        </div>
        <div class="accordion-item border-0 rounded-0 bg-transparent">
          <h2 class="accordion-header" id="two">
            <button class="accordion-button bg-transparent border-top border-bottom rounded-0 font-20 padding-y-24 collapsed text-transform-uppercase" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
              Arti Figur Sayap
            </button>
          </h2>
          <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="two" data-bs-parent="#faqAccordion">
            <div class="accordion-body">
              <p class="font-20 font-roboto opacity-50">Lorem ipsum dolor sit amet consectetur adipisicing elit. Consequuntur aut necessitatibus eos aliquam esse numquam eveniet aspernatur laboriosam explicabo at dignissimos quos quis eius expedita dolorum harum architecto iure, tempora voluptatem delectus porro qui veritatis consectetur! Esse nihil illo animi facilis culpa, laborum at veniam fugiat, odio ab error quas non reprehenderit perferendis, labore assumenda perspiciatis. Nesciunt unde recusandae nisi harum facilis inventore, sit nihil vitae ipsa. Corrupti officia expedita facere incidunt officiis, a sunt. Fugit nostrum autem iure. Cupiditate obcaecati magni officia dolorem voluptatum corrupti beatae. Facere mollitia iusto ut recusandae possimus excepturi, esse nulla ipsam, ullam vel saepe?</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?= $this->endSection() ?>