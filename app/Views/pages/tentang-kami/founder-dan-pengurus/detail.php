<?= $this->extend('layouts/base') ?>

<?= $this->section('append-style') ?>
<link rel="stylesheet" href="assets/css/pages/team.css">
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<section class="section min-height-300 margin-top-100 bg-light" id="section-page-team">
  <div class="team-wrapper">
    <div class="container container-no-xl">
      <div class="row m-0">
        <div class="col-md-5 p-0">
          <div class="team-image-wrapper h-100 overflow-hidden">
            <img src="<?= base_url() ?>/assets/img/sections/teams/one.jpg" alt=""  class="w-100 h-100 object-fit-cover">
          </div>
        </div>
        <div class="col-md-7 p-0">
          <div class="team-content-wrapper padding-x-96 padding-y-76">
            <div class="team-content-header">
              <img src="<?= base_url() ?>/assets/img/globals/logo-color.svg" alt="" width="auto" height="96">
              <h1 class="font-40 font-bold margin-top-64 mb-0">Tina T. Kemala Intan</h1>
              <p class="font-30 fonr-regular mb-0">Ketua</p>
            </div>
            <hr width="40%" class="margin-y-36">
            <div class="team-content-body">
              <p class="font-regular font-roboto font-20 opacity-50 line-height-35">Tugas dan tanggung jawab Ketua Srikandi BUMN adalah mengkoordinasikan dan mengorganisasikan seluruh penyelenggaraan organisasi dan program kerjanya serta mempertanggungjawabkan secara internal kepada Pengurus Srikandi serta kepada induk organisasi yaitu Forum Human Capital Indonesia (FHCI).</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="section min-height-1000 padding-y-56" id="section-page-team-content">
  <div class="team-content-wrapper">
    <div class="container">
      <div class="content-header">
        <h5 class="font-bold font-40">Tina T.<br>Kemala Intan</h5>
      </div>
      <div class="content-body margin-top-48">
        <div class="row margin-x-32-negative">
          <div class="col-lg-6 padding-x-32">
            <div class="content-body-left">
              <p class="line-height-35 font-20 font-regular font-roboto opacity-50">
                Alexandra Askandar, atau yang akrab disapa dengan Alexandra, menjabat sebagai Wakil Direktur Utama PT. Bank Mandiri (Persero) Tbk. sejak akhir Oktober 2020 yang lalu. Sebelum menjabat sebagai Wakil Direktur Utama, Alexandra pernah menjabat sebagai Direktur Corporate Banking (2019 – 2020) dan Direktur Hubungan Kelembagaan (2018 – 2019) Bank Mandiri.
                <br>
                Lulusan Sarjana Ekonomi dari UI dan MBA dari Boston University (Amerika Serikat) ini merupakan sosok srikandi yang telah berkarya di Bank Mandiri sejak tahun 2000. Alexandra mengawali karir nya sebagai Assistant Vice President dan melanjutkan karirnya sebagai Vice President di Coporate Banking hingga tahun 2008.
              </p>
              <a href="javascript:;" class="d-block text-dark font-22 opacity-50 margin-top-48">
                <i class="bi bi-linkedin"></i>
              </a>
            </div>
          </div>
          <div class="col-lg-6 padding-x-32">
            <div class="content-body-right border-bottom padding-bottom-24 margin-bottom-56">
              <div class="right-item">
                <div class="item-header">
                  <p class="font-roboto font-light font-20">Place, year of birth</p>
                </div>
                <div class="item-body">
                  <ul class="ps-3">
                    <li class="font-roboto font-20 font-regular">Medan, 1972</li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="content-body-right border-bottom padding-bottom-24 margin-bottom-56">
              <div class="right-item">
                <div class="item-header">
                  <p class="font-roboto font-light font-20">Education</p>
                </div>
                <div class="item-body">
                  <ul class="ps-3">
                    <li class="font-roboto font-20 font-regular">Bachelor's degree, University of Indonesia</li>
                    <li class="font-roboto font-20 font-regular">Master's degree, Boston University</li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="content-body-right padding-bottom-24 margin-bottom-56">
              <div class="right-item">
                <div class="item-header">
                  <p class="font-roboto font-light font-20">Experience</p>
                </div>
                <div class="item-body">
                  <ul class="ps-3">
                    <li class="font-roboto font-20 font-regular">Director of Corporate Banking, Bank Mandiri (2019-2020)</li>
                    <li class="font-roboto font-20 font-regular">Director of Institutional Relations, Bank Mandiri (2018 - 2019)</li>
                    <li class="font-roboto font-20 font-regular">Senior Executive Vice President Corporate Banking, Bank Mandiri (2016-2018)</li>
                    <li class="font-roboto font-20 font-regular">Commissioner, PT. Mandiri Sekuritas (2011 - 2018)</li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?= $this->endSection() ?>