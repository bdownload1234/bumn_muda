<?= $this->extend('layouts/base') ?>

<?= $this->section('append-style') ?>
<link rel="stylesheet" href="<?= base_url() ?>/assets/css/pages/fp-responsive.css">
<link rel="stylesheet" href="<?= base_url() ?>/assets/css/pages/founder-dan-pengurus.css">
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<section class="section-page-banner padding-y-128 bg-primary margin-top-116" id="section-page-banner">
  <div class="container">
    <div class="col-md-12 d-flex flex-wrap justify-content-between align-items-center">
      <div class="col-md-5">
        <div class="page-banner-header">
          <h1 class="font-40 font-bold text-white">
            Tentang<br>Srikandi BUMN
          </h1>
        </div>
      </div>
      <div class="col-md-4">
        <div class="page-banner-breadcrumb d-flex justify-content-end">
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item text-white opacity-50"><a href="#" class="text-decoration-none text-white">Home</a></li>
              <li class="breadcrumb-item text-white opacity-50"><a href="#" class="text-decoration-none text-white">Tentang Kami</a></li>
              <li class="breadcrumb-item active text-white" aria-current="page">Founder dan Pengurus</li>
            </ol>
          </nav>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="section-page-founder padding-y-80" id="section-page-founder">
  <div class="founder-wrapper">
    <div class="container">
      <div class="founder-content">
        <div class="founder-header text-center">
          <h5 class="font-40 font-bold">Founder Srikandi BUMN</h5>
          <p class="font-18 font-roboto font-regular margin-top-18">Srikandi BUMN dibentuk berdasarkan insiatif beberapa Karyawan dari 11 BUMN, yaitu:</p>
        </div>
        <div class="founder-body margin-top-64">
          <div class="row margin-x-9-negative">
            <div class="col-lg-4 col-md-6 margin-bottom-18 padding-x-9">
              <div class="founder-item bg-primary-brighter text-white h-100">
                <div class="d-flex align-items-center h-100">
                  <div class="item-number padding-left-24 padding-right-12 padding-y-24">
                    <span class="font-25 font-bold opacity-50">01</span>
                  </div>
                  <div class="item-content padding-y-24 padding-left-12 padding-right-24">
                    <h6 class="font-25 font-medium mb-0">A.A.A.Indira Pratyaksa</h6>
                    <p class="font-25 font-bold mb-0">PT Pertamina</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-6 margin-bottom-18 padding-x-9">
              <div class="founder-item bg-primary-brighter text-white h-100">
                <div class="d-flex align-items-center h-100">
                  <div class="item-number padding-left-24 padding-right-12 padding-y-24">
                    <span class="font-25 font-bold opacity-50">02</span>
                  </div>
                  <div class="item-content padding-y-24 padding-left-12 padding-right-24">
                    <h6 class="font-25 font-medium mb-0">Ami Novianti</h6>
                    <p class="font-25 font-bold mb-0">PT Angkasa Pura I</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-6 margin-bottom-18 padding-x-9">
              <div class="founder-item bg-primary-brighter text-white h-100">
                <div class="d-flex align-items-center h-100">
                  <div class="item-number padding-left-24 padding-right-12 padding-y-24">
                    <span class="font-25 font-bold opacity-50">03</span>
                  </div>
                  <div class="item-content padding-y-24 padding-left-12 padding-right-24">
                    <h6 class="font-25 font-medium mb-0">Citra Iriani</h6>
                    <p class="font-25 font-bold mb-0">PT Angkasa Pura I</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-6 margin-bottom-18 padding-x-9">
              <div class="founder-item bg-primary-brighter text-white h-100">
                <div class="d-flex align-items-center h-100">
                  <div class="item-number padding-left-24 padding-right-12 padding-y-24">
                    <span class="font-25 font-bold opacity-50">04</span>
                  </div>
                  <div class="item-content padding-y-24 padding-left-12 padding-right-24">
                    <h6 class="font-25 font-medium mb-0">Diah Karima</h6>
                    <p class="font-25 font-bold mb-0">PT PLN</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-6 margin-bottom-18 padding-x-9">
              <div class="founder-item bg-primary-brighter text-white h-100">
                <div class="d-flex align-items-center h-100">
                  <div class="item-number padding-left-24 padding-right-12 padding-y-24">
                    <span class="font-25 font-bold opacity-50">05</span>
                  </div>
                  <div class="item-content padding-y-24 padding-left-12 padding-right-24">
                    <h6 class="font-25 font-medium mb-0">Dina Linarti</h6>
                    <p class="font-25 font-bold mb-0">PT Bank Tabungan Negara</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-6 margin-bottom-18 padding-x-9">
              <div class="founder-item bg-primary-brighter text-white h-100">
                <div class="d-flex align-items-center h-100">
                  <div class="item-number padding-left-24 padding-right-12 padding-y-24">
                    <span class="font-25 font-bold opacity-50">06</span>
                  </div>
                  <div class="item-content padding-y-24 padding-left-12 padding-right-24">
                    <h6 class="font-25 font-medium mb-0">Dwi Normasari</h6>
                    <p class="font-25 font-bold mb-0">PT Bank Rakyat Indonesia</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-6 margin-bottom-18 padding-x-9">
              <div class="founder-item bg-primary-brighter text-white h-100">
                <div class="d-flex align-items-center h-100">
                  <div class="item-number padding-left-24 padding-right-12 padding-y-24">
                    <span class="font-25 font-bold opacity-50">07</span>
                  </div>
                  <div class="item-content padding-y-24 padding-left-12 padding-right-24">
                    <h6 class="font-25 font-medium mb-0">Eka Setia Utami</h6>
                    <p class="font-25 font-bold mb-0">PT Bank Negara Indonesia</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-6 margin-bottom-18 padding-x-9">
              <div class="founder-item bg-primary-brighter text-white h-100">
                <div class="d-flex align-items-center h-100">
                  <div class="item-number padding-left-24 padding-right-12 padding-y-24">
                    <span class="font-25 font-bold opacity-50">08</span>
                  </div>
                  <div class="item-content padding-y-24 padding-left-12 padding-right-24">
                    <h6 class="font-25 font-medium mb-0">Eko Waluyo </h6>
                    <p class="font-25 font-bold mb-0">PT Bank Tabungan Negara</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-6 margin-bottom-18 padding-x-9">
              <div class="founder-item bg-primary-brighter text-white h-100">
                <div class="d-flex align-items-center h-100">
                  <div class="item-number padding-left-24 padding-right-12 padding-y-24">
                    <span class="font-25 font-bold opacity-50">09</span>
                  </div>
                  <div class="item-content padding-y-24 padding-left-12 padding-right-24">
                    <h6 class="font-25 font-medium mb-0">Elminda Sari</h6>
                    <p class="font-25 font-bold mb-0">PT Bank Mandiri</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-6 margin-bottom-18 padding-x-9">
              <div class="founder-item bg-primary-brighter text-white h-100">
                <div class="d-flex align-items-center h-100">
                  <div class="item-number padding-left-24 padding-right-12 padding-y-24">
                    <span class="font-25 font-bold opacity-50">10</span>
                  </div>
                  <div class="item-content padding-y-24 padding-left-12 padding-right-24">
                    <h6 class="font-25 font-medium mb-0">Erlisetya Wahyudi</h6>
                    <p class="font-25 font-bold mb-0">PT ASDP Indonesia Ferry</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-6 margin-bottom-18 padding-x-9">
              <div class="founder-item bg-primary-brighter text-white h-100">
                <div class="d-flex align-items-center h-100">
                  <div class="item-number padding-left-24 padding-right-12 padding-y-24">
                    <span class="font-25 font-bold opacity-50">11</span>
                  </div>
                  <div class="item-content padding-y-24 padding-left-12 padding-right-24">
                    <h6 class="font-25 font-medium mb-0">Fajriyah Usman</h6>
                    <p class="font-25 font-bold mb-0">PT Pertamina</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-6 margin-bottom-18 padding-x-9">
              <div class="founder-item bg-primary-brighter text-white h-100">
                <div class="d-flex align-items-center h-100">
                  <div class="item-number padding-left-24 padding-right-12 padding-y-24">
                    <span class="font-25 font-bold opacity-50">12</span>
                  </div>
                  <div class="item-content padding-y-24 padding-left-12 padding-right-24">
                    <h6 class="font-25 font-medium mb-0">Fatmi Nurita </h6>
                    <p class="font-25 font-bold mb-0">PNRI</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-6 margin-bottom-18 padding-x-9">
              <div class="founder-item bg-primary-brighter text-white h-100">
                <div class="d-flex align-items-center h-100">
                  <div class="item-number padding-left-24 padding-right-12 padding-y-24">
                    <span class="font-25 font-bold opacity-50">13</span>
                  </div>
                  <div class="item-content padding-y-24 padding-left-12 padding-right-24">
                    <h6 class="font-25 font-medium mb-0">Fitra Maria Phitaloka</h6>
                    <p class="font-25 font-bold mb-0">PT Bank Negara Indonesia</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-6 margin-bottom-18 padding-x-9">
              <div class="founder-item bg-primary-brighter text-white h-100">
                <div class="d-flex align-items-center h-100">
                  <div class="item-number padding-left-24 padding-right-12 padding-y-24">
                    <span class="font-25 font-bold opacity-50">14</span>
                  </div>
                  <div class="item-content padding-y-24 padding-left-12 padding-right-24">
                    <h6 class="font-25 font-medium mb-0">Imam Suhadi</h6>
                    <p class="font-25 font-bold mb-0">PT Telkom Indonesia</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-6 margin-bottom-18 padding-x-9">
              <div class="founder-item bg-primary-brighter text-white h-100">
                <div class="d-flex align-items-center h-100">
                  <div class="item-number padding-left-24 padding-right-12 padding-y-24">
                    <span class="font-25 font-bold opacity-50">15</span>
                  </div>
                  <div class="item-content padding-y-24 padding-left-12 padding-right-24">
                    <h6 class="font-25 font-medium mb-0">Indri L Juwono</h6>
                    <p class="font-25 font-bold mb-0">Perum Damri</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-6 margin-bottom-18 padding-x-9">
              <div class="founder-item bg-primary-brighter text-white h-100">
                <div class="d-flex align-items-center h-100">
                  <div class="item-number padding-left-24 padding-right-12 padding-y-24">
                    <span class="font-25 font-bold opacity-50">16</span>
                  </div>
                  <div class="item-content padding-y-24 padding-left-12 padding-right-24">
                    <h6 class="font-25 font-medium mb-0">Maya Windarti</h6>
                    <p class="font-25 font-bold mb-0">PT PLN</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-6 margin-bottom-18 padding-x-9">
              <div class="founder-item bg-primary-brighter text-white h-100">
                <div class="d-flex align-items-center h-100">
                  <div class="item-number padding-left-24 padding-right-12 padding-y-24">
                    <span class="font-25 font-bold opacity-50">17</span>
                  </div>
                  <div class="item-content padding-y-24 padding-left-12 padding-right-24">
                    <h6 class="font-25 font-medium mb-0">Melia Pramadhona </h6>
                    <p class="font-25 font-bold mb-0">PT Bank Rakyat Indonesia</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-6 margin-bottom-18 padding-x-9">
              <div class="founder-item bg-primary-brighter text-white h-100">
                <div class="d-flex align-items-center h-100">
                  <div class="item-number padding-left-24 padding-right-12 padding-y-24">
                    <span class="font-25 font-bold opacity-50">18</span>
                  </div>
                  <div class="item-content padding-y-24 padding-left-12 padding-right-24">
                    <h6 class="font-25 font-medium mb-0">Mila Ratnasari</h6>
                    <p class="font-25 font-bold mb-0">PT Pertamina</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-6 margin-bottom-18 padding-x-9">
              <div class="founder-item bg-primary-brighter text-white h-100">
                <div class="d-flex align-items-center h-100">
                  <div class="item-number padding-left-24 padding-right-12 padding-y-24">
                    <span class="font-25 font-bold opacity-50">19</span>
                  </div>
                  <div class="item-content padding-y-24 padding-left-12 padding-right-24">
                    <h6 class="font-25 font-medium mb-0">Nova Selvianty Masril </h6>
                    <p class="font-25 font-bold mb-0">PT Bank Rakyat Indonesia</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-6 margin-bottom-18 padding-x-9">
              <div class="founder-item bg-primary-brighter text-white h-100">
                <div class="d-flex align-items-center h-100">
                  <div class="item-number padding-left-24 padding-right-12 padding-y-24">
                    <span class="font-25 font-bold opacity-50">20</span>
                  </div>
                  <div class="item-content padding-y-24 padding-left-12 padding-right-24">
                    <h6 class="font-25 font-medium mb-0">Pra Ulpa Treeda</h6>
                    <p class="font-25 font-bold mb-0">PT Bank Mandiri</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-6 margin-bottom-18 padding-x-9">
              <div class="founder-item bg-primary-brighter text-white h-100">
                <div class="d-flex align-items-center h-100">
                  <div class="item-number padding-left-24 padding-right-12 padding-y-24">
                    <span class="font-25 font-bold opacity-50">21</span>
                  </div>
                  <div class="item-content padding-y-24 padding-left-12 padding-right-24">
                    <h6 class="font-25 font-medium mb-0">Prasetyorini</h6>
                    <p class="font-25 font-bold mb-0">PT PLN</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-6 margin-bottom-18 padding-x-9">
              <div class="founder-item bg-primary-brighter text-white h-100">
                <div class="d-flex align-items-center h-100">
                  <div class="item-number padding-left-24 padding-right-12 padding-y-24">
                    <span class="font-25 font-bold opacity-50">22</span>
                  </div>
                  <div class="item-content padding-y-24 padding-left-12 padding-right-24">
                    <h6 class="font-25 font-medium mb-0">Prihatiningsih</h6>
                    <p class="font-25 font-bold mb-0">PT Pertamina</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-6 margin-bottom-18 padding-x-9">
              <div class="founder-item bg-primary-brighter text-white h-100">
                <div class="d-flex align-items-center h-100">
                  <div class="item-number padding-left-24 padding-right-12 padding-y-24">
                    <span class="font-25 font-bold opacity-50">23</span>
                  </div>
                  <div class="item-content padding-y-24 padding-left-12 padding-right-24">
                    <h6 class="font-25 font-medium mb-0">Rahmayati </h6>
                    <p class="font-25 font-bold mb-0">PT Bank Tabungan Negara</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-6 margin-bottom-18 padding-x-9">
              <div class="founder-item bg-primary-brighter text-white h-100">
                <div class="d-flex align-items-center h-100">
                  <div class="item-number padding-left-24 padding-right-12 padding-y-24">
                    <span class="font-25 font-bold opacity-50">24</span>
                  </div>
                  <div class="item-content padding-y-24 padding-left-12 padding-right-24">
                    <h6 class="font-25 font-medium mb-0">Riesa Callista Surahman</h6>
                    <p class="font-25 font-bold mb-0">PT ASDP Indonesia Ferry</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-6 margin-bottom-18 padding-x-9">
              <div class="founder-item bg-primary-brighter text-white h-100">
                <div class="d-flex align-items-center h-100">
                  <div class="item-number padding-left-24 padding-right-12 padding-y-24">
                    <span class="font-25 font-bold opacity-50">25</span>
                  </div>
                  <div class="item-content padding-y-24 padding-left-12 padding-right-24">
                    <h6 class="font-25 font-medium mb-0">Savitri</h6>
                    <p class="font-25 font-bold mb-0">PT Bank Negara Indonesia</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-6 margin-bottom-18 padding-x-9">
              <div class="founder-item bg-primary-brighter text-white h-100">
                <div class="d-flex align-items-center h-100">
                  <div class="item-number padding-left-24 padding-right-12 padding-y-24">
                    <span class="font-25 font-bold opacity-50">26</span>
                  </div>
                  <div class="item-content padding-y-24 padding-left-12 padding-right-24">
                    <h6 class="font-25 font-medium mb-0">Siti Inda Suri</h6>
                    <p class="font-25 font-bold mb-0">Perum Damri</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-6 margin-bottom-18 padding-x-9">
              <div class="founder-item bg-primary-brighter text-white h-100">
                <div class="d-flex align-items-center h-100">
                  <div class="item-number padding-left-24 padding-right-12 padding-y-24">
                    <span class="font-25 font-bold opacity-50">27</span>
                  </div>
                  <div class="item-content padding-y-24 padding-left-12 padding-right-24">
                    <h6 class="font-25 font-medium mb-0">Sri Saptadi</h6>
                    <p class="font-25 font-bold mb-0">PT Telkom Indonesia</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-6 margin-bottom-18 padding-x-9">
              <div class="founder-item bg-primary-brighter text-white h-100">
                <div class="d-flex align-items-center h-100">
                  <div class="item-number padding-left-24 padding-right-12 padding-y-24">
                    <span class="font-25 font-bold opacity-50">28</span>
                  </div>
                  <div class="item-content padding-y-24 padding-left-12 padding-right-24">
                    <h6 class="font-25 font-medium mb-0">Sri Widiyati </h6>
                    <p class="font-25 font-bold mb-0">PT Bank Rakyat Indonesia</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-6 margin-bottom-18 padding-x-9">
              <div class="founder-item bg-primary-brighter text-white h-100">
                <div class="d-flex align-items-center h-100">
                  <div class="item-number padding-left-24 padding-right-12 padding-y-24">
                    <span class="font-25 font-bold opacity-50">29</span>
                  </div>
                  <div class="item-content padding-y-24 padding-left-12 padding-right-24">
                    <h6 class="font-25 font-medium mb-0">Widia Dian Wulandari</h6>
                    <p class="font-25 font-bold mb-0">PT Telkom Indonesia</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-6 margin-bottom-18 padding-x-9">
              <div class="founder-item bg-primary-brighter text-white h-100">
                <div class="d-flex align-items-center h-100">
                  <div class="item-number padding-left-24 padding-right-12 padding-y-24">
                    <span class="font-25 font-bold opacity-50">30</span>
                  </div>
                  <div class="item-content padding-y-24 padding-left-12 padding-right-24">
                    <h6 class="font-25 font-medium mb-0">Yeni widiastuti </h6>
                    <p class="font-25 font-bold mb-0">PNRI</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-6 margin-bottom-18 padding-x-9">
              <div class="founder-item bg-primary-brighter text-white h-100">
                <div class="d-flex align-items-center h-100">
                  <div class="item-number padding-left-24 padding-right-12 padding-y-24">
                    <span class="font-25 font-bold opacity-50">31</span>
                  </div>
                  <div class="item-content padding-y-24 padding-left-12 padding-right-24">
                    <h6 class="font-25 font-medium mb-0">Yenni Sari Dewi</h6>
                    <p class="font-25 font-bold mb-0">PT Bank Negara Indonesia</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="section-page-struktur padding-y-80 bg-light" id="section-page-struktur">
  <div class="struktur-wrapper">
    <div class="container">
      <div class="struktur-content">
        <div class="struktur-header text-center">
          <h5 class="font-40 font-bold">Struktur Organisasi Srikandi BUMN</h5>
          <p class="font-18 font-roboto font-regular margin-top-18">Excellent people, Excellent bussiness</p>
        </div>
        <div class="founder-body text-center margin-top-128">
          <img src="<?= base_url() ?>/assets/img/globals/struktur-srikandi-bumn.png" alt="" class="w-75" style="mix-blend-mode: multiply;">
        </div>
      </div>
    </div>
  </div>
</section>

<section class="section-page-pengurus padding-y-80" id="section-page-pengurus">
  <div class="pengurus-wrapper">
    <div class="container">
      <div class="prngurus-content">
        <div class="pengurus-header text-center">
          <h5 class="font-40 font-bold">Pengurus Srikandi BUMN<br>Periode 2021 - 2024</h5>
          <p class="font-18 font-roboto font-regular margin-top-18">Pengukuhan Ketua Srikandi BUMN, Ibu Tina Kemala Intan dilakukan bersamaan dengan Ketua Umum FHCI dan Pengurus FHCI periode 2021-2024, yaitu pada 7 April 2021 oleh Menteri BUMN, Erick Thohir.</p>
        </div>
        <div class="pengurus-body margin-top-80">
          <div class="srikandi-team-body">
            <div class="row margin-x-24-negative">
              <div class="col-xl-3 col-lg-4 col-md-6 padding-x-24 mx-auto">
                <div class="srikandi-team-item margin-bottom-48 h-100">
                  <div class="srikandi-team-image-wrapper">
                    <div class="team-image text-center">
                      <img src="<?= base_url() ?>/assets/img/sections/teams/1.jpg" alt="" class="img-fluid">
                    </div>
                    <div class="team-description padding-x-24 padding-y-40 bg-primary h-100 text-white d-flex flex-column justify-content-between">
                      <p class="font-roboto font-16 font-regular mb-0">
                        Tugas dan tanggungjawab Ketua Srikandi BUMN adalah mengkoordinasikan dan mengorganisasikan seluruh penyelenggaraan organisasi dan program kerjanya serta mempertanggungjawabkan secara internal kepada Pengurus Srikandi serta kepada induk organisasi yaitu Forum Human Capital Indonesia (FHCI).
                      </p>
                      <div class="d-flex justify-content-between align-items-center">
                        <a href="<?= base_url() ?>/tentang-kami/founder-dan-pengurus/..." class="btn btn-outline-light rounded-pill padding-y-8 font-regular padding-x-18 font-12">
                          <i class="bi bi-eye me-2"></i>
                          Profil Lengkap
                        </a>
                        <a href="javascript:;" class="d-block text-white font-22">
                          <i class="bi bi-linkedin"></i>
                        </a>
                      </div>
                    </div>
                  </div>
                  <div class="srikandi-team-content text-center">
                    <h6 class="font-20 font-bold margin-top-24 mb-0">Tina T. Ke mala Intan</h6>
                    <p class="text-transform-uppercase margin-y-8">Ketua</p>
                    <small class="opacity-50">Direktur SDM dan Hukum PT Semen Indonesia (Persero)</small>
                  </div>
                </div>
              </div>
            </div>
            <div class="row margin-x-24-negative">
              <div class="col-xl-3 col-lg-4 col-md-6 padding-x-24">
                <div class="srikandi-team-item margin-bottom-48 h-100">
                  <div class="srikandi-team-image-wrapper">
                    <div class="team-image text-center">
                      <img src="<?= base_url() ?>/assets/img/sections/teams/2.jpg" alt="" class="img-fluid">
                    </div>
                    <div class="team-description padding-x-24 padding-y-40 bg-primary h-100 text-white d-flex flex-column justify-content-between">
                      <p class="font-roboto font-16 font-regular mb-0">
                        Tugas dan tanggungjawab Ketua Srikandi BUMN adalah mengkoordinasikan dan mengorganisasikan seluruh penyelenggaraan organisasi dan program kerjanya serta mempertanggungjawabkan secara internal kepada Pengurus Srikandi serta kepada induk organisasi yaitu Forum Human Capital Indonesia (FHCI).
                      </p>
                      <div class="d-flex justify-content-between align-items-center">
                        <a href="<?= base_url() ?>/tentang-kami/founder-dan-pengurus/..." class="btn btn-outline-light rounded-pill padding-y-8 font-regular padding-x-18 font-12">
                          <i class="bi bi-eye me-2"></i>
                          Profil Lengkap
                        </a>
                        <a href="javascript:;" class="d-block text-white font-22">
                          <i class="bi bi-linkedin"></i>
                        </a>
                      </div>
                    </div>
                  </div>
                  <div class="srikandi-team-content text-center">
                    <h6 class="font-20 font-bold margin-top-24 mb-0">Judith J. Dipodiputro</h6>
                    <p class="text-transform-uppercase margin-y-8">Sekretaris Jenderal</p>
                    <small class="opacity-50">Direktur Utama Perum Produksi Film Negara</small>
                  </div>
                </div>
              </div>
              <div class="col-xl-3 col-lg-4 col-md-6 padding-x-24">
                <div class="srikandi-team-item margin-bottom-48 h-100">
                  <div class="srikandi-team-image-wrapper">
                    <div class="team-image text-center">
                      <img src="<?= base_url() ?>/assets/img/sections/teams/3.jpg" alt="" class="img-fluid">
                    </div>
                    <div class="team-description padding-x-24 padding-y-40 bg-primary h-100 text-white d-flex flex-column justify-content-between">
                      <p class="font-roboto font-16 font-regular mb-0">
                        Tugas dan tanggungjawab Ketua Srikandi BUMN adalah mengkoordinasikan dan mengorganisasikan seluruh penyelenggaraan organisasi dan program kerjanya serta mempertanggungjawabkan secara internal kepada Pengurus Srikandi serta kepada induk organisasi yaitu Forum Human Capital Indonesia (FHCI).
                      </p>
                      <div class="d-flex justify-content-between align-items-center">
                        <a href="<?= base_url() ?>/tentang-kami/founder-dan-pengurus/..." class="btn btn-outline-light rounded-pill padding-y-8 font-regular padding-x-18 font-12">
                          <i class="bi bi-eye me-2"></i>
                          Profil Lengkap
                        </a>
                        <a href="javascript:;" class="d-block text-white font-22">
                          <i class="bi bi-linkedin"></i>
                        </a>
                      </div>
                    </div>
                  </div>
                  <div class="srikandi-team-content text-center">
                    <h6 class="font-20 font-bold margin-top-24 mb-0">Dewi Aryani Suzana</h6>
                    <p class="text-transform-uppercase margin-y-8">Wakil Sekretaris Jenderal</p>
                    <small class="opacity-50">Direktur SDM & Umum Jasa Raharja</small>
                  </div>
                </div>
              </div>
              <div class="col-xl-3 col-lg-4 col-md-6 padding-x-24">
                <div class="srikandi-team-item margin-bottom-48 h-100">
                  <div class="srikandi-team-image-wrapper">
                    <div class="team-image text-center">
                      <img src="<?= base_url() ?>/assets/img/sections/teams/4.jpg" alt="" class="img-fluid">
                    </div>
                    <div class="team-description padding-x-24 padding-y-40 bg-primary h-100 text-white d-flex flex-column justify-content-between">
                      <p class="font-roboto font-16 font-regular mb-0">
                        Tugas dan tanggungjawab Ketua Srikandi BUMN adalah mengkoordinasikan dan mengorganisasikan seluruh penyelenggaraan organisasi dan program kerjanya serta mempertanggungjawabkan secara internal kepada Pengurus Srikandi serta kepada induk organisasi yaitu Forum Human Capital Indonesia (FHCI).
                      </p>
                      <div class="d-flex justify-content-between align-items-center">
                        <a href="<?= base_url() ?>/tentang-kami/founder-dan-pengurus/..." class="btn btn-outline-light rounded-pill padding-y-8 font-regular padding-x-18 font-12">
                          <i class="bi bi-eye me-2"></i>
                          Profil Lengkap
                        </a>
                        <a href="javascript:;" class="d-block text-white font-22">
                          <i class="bi bi-linkedin"></i>
                        </a>
                      </div>
                    </div>
                  </div>
                  <div class="srikandi-team-content text-center">
                    <h6 class="font-20 font-bold margin-top-24 mb-0">Winarsih Budiriani</h6>
                    <p class="text-transform-uppercase margin-y-8">Bendahara</p>
                    <small class="opacity-50">Direktur Keuangan dan Manajemen Risiko Perum Percetakan Uang Republik Indonesia (PERURI)</small>
                  </div>
                </div>
              </div>
              <div class="col-xl-3 col-lg-4 col-md-6 padding-x-24">
                <div class="srikandi-team-item margin-bottom-48 h-100">
                  <div class="srikandi-team-image-wrapper">
                    <div class="team-image text-center">
                      <img src="<?= base_url() ?>/assets/img/sections/teams/5.jpg" alt="" class="img-fluid">
                    </div>
                    <div class="team-description padding-x-24 padding-y-40 bg-primary h-100 text-white d-flex flex-column justify-content-between">
                      <p class="font-roboto font-16 font-regular mb-0">
                        Tugas dan tanggungjawab Ketua Srikandi BUMN adalah mengkoordinasikan dan mengorganisasikan seluruh penyelenggaraan organisasi dan program kerjanya serta mempertanggungjawabkan secara internal kepada Pengurus Srikandi serta kepada induk organisasi yaitu Forum Human Capital Indonesia (FHCI).
                      </p>
                      <div class="d-flex justify-content-between align-items-center">
                        <a href="<?= base_url() ?>/tentang-kami/founder-dan-pengurus/..." class="btn btn-outline-light rounded-pill padding-y-8 font-regular padding-x-18 font-12">
                          <i class="bi bi-eye me-2"></i>
                          Profil Lengkap
                        </a>
                        <a href="javascript:;" class="d-block text-white font-22">
                          <i class="bi bi-linkedin"></i>
                        </a>
                      </div>
                    </div>
                  </div>
                  <div class="srikandi-team-content text-center">
                    <h6 class="font-20 font-bold margin-top-24 mb-0">Merlany Legitasari</h6>
                    <p class="text-transform-uppercase margin-y-8">Wakil Bendahara</p>
                    <small class="opacity-50">Direktur PT Virama Karya (Persero)</small>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?= $this->endSection() ?>