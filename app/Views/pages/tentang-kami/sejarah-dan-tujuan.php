<?= $this->extend('layouts/base') ?>

<?= $this->section('append-style') ?>
<link rel="stylesheet" href="<?= base_url() ?>/assets/css/pages/st-responsive.css">
<link rel="stylesheet" href="<?= base_url() ?>/assets/css/pages/sejarah-dan-tujuan.css">
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<section class="section-page-banner padding-y-128 bg-primary margin-top-116" id="section-page-banner">
  <div class="container">
    <div class="col-md-12 d-flex flex-wrap justify-content-between align-items-center">
      <div class="col-md-5">
        <div class="page-banner-header">
          <h1 class="font-40 font-bold text-white">
            Tentang<br>Srikandi BUMN
          </h1>
        </div>
      </div>
      <div class="col-md-4">
        <div class="page-banner-breadcrumb d-flex justify-content-end">
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item text-white opacity-50"><a href="#" class="text-decoration-none text-white">Home</a></li>
              <li class="breadcrumb-item text-white opacity-50"><a href="#" class="text-decoration-none text-white">Tentang Kami</a></li>
              <li class="breadcrumb-item active text-white" aria-current="page">Sejarah dan Tujuan</li>
            </ol>
          </nav>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="section-page-history padding-y-148" id="section-page-history">
  <div class="container">
    <div class="page-history-content">
      <div class="history-content-header">
        <h5 class="font-40 font-bold mb-0">Sejarah Srikandi <br>BUMN</h5>
      </div>
      <div class="history-content-body margin-top-80">
        <p class="font-roboto font-18 line-height-33 font-regular">
          Srikandi BUMN merupakan komunitas perempuan berkarya di BUMN untuk saling mendukung sesama perempuan dalam berkarya dan berprestasi dengan beragam peran sebagai ibu, istri dan pekerja. Komunitas Srikandi BUMN didirikan untuk menjawab panggilan Bpk. Joko Widodo, Presiden Republik Indonesia yang juga Impact Champion for HeforShe Program United Nations, yang menyatakan bahwa Indonesia berkomitmen untuk meningkatkan keterwakilan perempuan hingga 30% di parlemen dan berbagai pembuat kebijakan. Hal ini juga diperkuat dengan adanya arahan dari Bpk. Erick Thohir, selaku Menteri BUMN Republik Indonesia, bahwa BUMN punya program ke depan untuk meningkatkan jumlah perempuan, kalau bisa 15% di tingkat direksi BUMN, sesuai standar Asia Tenggara.
          <br><br>
          Komunitas Srikandi BUMN didirikan dengan kesadaran bahwa perempuan mempunyai kesempatan yang sama untuk bekerja dan berkarir. Hal ini dibuktikan oleh data bahwa rata-rata prosentase penerimaan karyawan laki-laki dan perempuan adalah sama, tetapi sekitar 55% karyawan perempuan mengundurkan diri pada awal karir nya (< 5 tahun). Tiga alasan utama karyawan perempuan mengundurkan diri pada umumnya karena urusan internal keluarga, rata-rata dengan alasan: mengurus anak khususnya yang masih usia balita; tidak diizinkan suami bekerja atau diperbolehkan bekerja tapi dengan keterikatan yang membuat perempuan lebih memilih untuk tidak bekerja, serta kebutuhan untuk mengurus orang tua yang sakit atau berusia lanjut.
          <br><br>
          Sedangkan kondisi ataupun penerimaan perempuan bekerja di lingkungan kantor juga banyak yang tidak terlepas dari gender bias yaitu pandangan sebagian orang bahwa terdapat perbedaan kemampuan antara laki-laki dan perempuan yang berdampak pada perbedaan kesempatan karir. Adanya gender bias ini membuat pengambilan keputusan pada saat promosi atapun penempatan rotasi unconsciously memprioritaskan karyawan laki-laki karena persepsi bahwa karyawan perempuan akan sulit untuk dipromosikan ke jenjang yang lebih tinggi atau dirotasi karena faktor keluarga. Gender bias juga membuat karyawan perempuan hanya diarahkan untuk mengembangkan diri pada pekerjaan yang dinilai baik pada bidang-bidang yang woman minded.
          <br><br>
          Pengambilan keputusan yang unconscious bias memihak pada gender laki-laki sering terjadi karena paradigma bahwa karyawan perempuan memilki banyak keterbatasan, seperti harus cuti melahirkan 3 bulan dan kondisi sedang menyusui. Dalam hal ini, diperlukan dispensasi ataupun kompensasi yang lebih banyak untuk karyawan perempuan. Sedangkan karyawan perempuan merasa belum mendapatkan dukungan infrastruktur dari perusahaan yang belum mencukupi bagi karyawan perempuan untuk berkarir, seperti ruang menyusui ataupun penyediaan fasilitas penitipan anak agar karyawan perempuan dapat dengan tenang bekerja.
          <br><br>
          Di sisi perempuan sendiri, mereka juga cenderung melakukan pembatasan dengan mencukupkan diri pada tingkat jabatan tertentu dengan pertimbangan keseimbangan karir dan keluarga serta dukungan yang kurang maksimal dari keluarga.
        </p>
      </div>
    </div>
  </div>
</section>


<section class="section-page-about padding-y-148 bg-light" id="section-page-about">
  <div class="container">
    <div class="row margin-x-48-negative flex-row-reverse">
      <div class="col-md-6 padding-x-48">
        <div class="page-about-image">
          <img src="<?= base_url() ?>/assets/img/sections/about-1.jpg" alt="" class="img-fluid padding-left-48 w-100">
          <img src="<?= base_url() ?>/assets/img/sections/about-1-before.jpg" alt="" class="img-fluid w-100 padding-right-48 margin-top-48">
        </div>
      </div>
      <div class="col-md-6 padding-x-48">
        <div class="page-about-content">
          <div class="about-content-header">
            <h5 class="font-40 font-bold mb-0 padding-top-md-28">Tujuan Srikandi <br>BUMN</h5>
          </div>
          <div class="about-content-body margin-y-36">
            <p class="font-roboto font-18 line-height-33 font-regular">
              Komunitas SrikandiBUMN didirikan untuk memberikan wadah bagi perempuan berkarya di BUMN agar dapat saling mendukung, membangun personal and professional capability, ingin terus menerus belajar hal-hal yang baru, mampu beradaptasi dengan berbagai perubahan, serta menjaga keseimbangan dengan perannya sebagai ibu dan istri.
              <br><br>
              Komunitas SrikandiBUMN tidak didirikan untuk mendapatkan hak istimewa agar perempuan mendapatkan fasilitas atau perhatian yang lebih baik dibandingkan karyawan laki-laki. Karena SrikandiBUMN menyadari bahwa secara professional, kinerja dan kompetensi adalah faktor utama yang dijadikan dasar dalam perkembangan karir seseorang.
              <br><br>
              SrikandiBUMN bertujuan untuk membangun kesadaran semua pihak tentang perkembangan karir berbasis kompetensi dan performansi karyawan, tidak ada perbedaan kesempatan karena gender. Hal ini bisa dilakukan jika dibangun talent management system yang transparan dan fair untuk memberikan kesetaraan kesempatan karir yang tidak bias gender.
            </p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?= $this->endSection() ?>