<?php

namespace App\Controllers;

class Home extends BaseController
{
    public function index()
    {
        $data['blogs'] = json_decode($this->api([
            'url' => $this->apiUrl . '/blog',
            'method' => 'GET',
            'header' => 'application/json',
        ]))->artikel;
        $data['events'] = json_decode($this->api([
            'url' => $this->apiUrl . '/event',
            'method' => 'GET',
            'header' => 'application/json',
        ]))->event;
        return view('pages/home/index', $data);
    }
}
