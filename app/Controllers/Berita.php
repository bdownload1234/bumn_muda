<?php

namespace App\Controllers;

use App\Controllers\BaseController;

class Berita extends BaseController
{
    public function index()
    {
        return view('pages/berita/index');
    }

    public function artikel_dan_event()
    {
        return view('pages/berita/artikel-dan-event');
    }

    public function figur()
    {
        return view('pages/berita/figur');
    }
}
