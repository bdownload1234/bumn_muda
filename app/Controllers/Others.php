<?php

namespace App\Controllers;

use App\Controllers\BaseController;

class Others extends BaseController
{
    public function index()
    {
        return redirect()->to('/');
    }

    public function subscribe() {
        if($this->request->isAJAX()) {
            $return = $this->api([
                'url' => $this->apiUrl . '/subscriber',
                'method' => 'POST',
                'header' => 'application/json',
                'body' => [
                    'name' => $this->request->getPost('subscribe_name'),
                    'email' => $this->request->getPost('subscribe_email')
                ],
            ]);
            return $this->response->setJSON($return);
        }
    }
}
