<?php

namespace App\Controllers;

use App\Controllers\BaseController;

class Program extends BaseController
{
    public function index()
    {
        return redirect()->to('/');
    }

    public function kajian_dan_pendampingan_perempuan()
    {
        return view('pages/program/kajian-dan-pendampingan-perempuan');
    }

    public function pendidikan_dan_pengembangan()
    {
        return view('pages/program/pendidikan-dan-pengembangan');
    }

    public function kesehatan_dan_kesejahteraan()
    {
        return view('pages/program/kesehatan-dan-kesejahteraan');
    }

    public function bidang_komunikasi_dan_kerjasama()
    {
        return view('pages/program/bidang-komunikasi-dan-kerjasama');
    }

    public function sosial_masyarakat()
    {
        return view('pages/program/sosial-masyarakat');
    }
}
