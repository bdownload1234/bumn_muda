<?php

namespace App\Controllers;

use App\Controllers\BaseController;

class Tentang extends BaseController
{
    public function index()
    {
        return redirect()->to('/');
    }

    public function sejarah_dan_tujuan()
    {
        return view('pages/tentang-kami/sejarah-dan-tujuan');
    }

    public function arti_logo()
    {
        return view('pages/tentang-kami/arti-logo');
    }

    public function founder_dan_pengurus($any = NULL)
    {
        if($any) {
            $data['team'] = json_decode($this->api([
                'url' => $this->apiUrl . '/team/' . $any,
                'method' => 'GET',
                'header' => 'application/json',
            ]));
            return view('pages/tentang-kami/founder-dan-pengurus/detail', $data);
        } else {
            return view('pages/tentang-kami/founder-dan-pengurus/index');
        }
    }
}
