<?php

namespace App\Controllers;

use App\Controllers\BaseController;

class Galeri extends BaseController
{
    public function index()
    {
        return view('pages/galeri/index');
    }
}
