<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (is_file(SYSTEMPATH . 'Config/Routes.php')) {
    require SYSTEMPATH . 'Config/Routes.php';
}

/*
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
// The Auto Routing (Legacy) is very dangerous. It is easy to create vulnerable apps
// where controller filters or CSRF protection are bypassed.
// If you don't want to define all routes, please use the Auto Routing (Improved).
// Set `$autoRoutesImproved` to true in `app/Config/Feature.php` and set the following to true.
//$routes->setAutoRoute(false);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/', 'Home::index');
$routes->group('/tentang-kami', function($routes) {
    $routes->get('/', 'Tentang::index');
    $routes->get('sejarah-dan-tujuan', 'Tentang::sejarah_dan_tujuan');
    $routes->get('arti-logo', 'Tentang::arti_logo');
    $routes->group('founder-dan-pengurus', function($routes) {
        $routes->get('/', 'Tentang::founder_dan_pengurus');
        $routes->get('(:any)', 'Tentang::founder_dan_pengurus/$1');
    });
});
$routes->group('/program', function($routes) {
    $routes->get('/', 'Program::index');
    $routes->get('kajian-dan-pendampingan-perempuan', 'Program::kajian_dan_pendampingan_perempuan');
    $routes->get('pendidikan-dan-pengembangan', 'Program::pendidikan_dan_pengembangan');
    $routes->get('kesehatan-dan-kesejahteraan', 'Program::kesehatan_dan_kesejahteraan');
    $routes->get('bidang-komunikasi-dan-kerjasama', 'Program::bidang_komunikasi_dan_kerjasama');
    $routes->get('sosial-masyarakat', 'Program::sosial_masyarakat');
});
$routes->group('/berita', function($routes) {
    $routes->get('/', 'Berita::index');
    $routes->get('artikel-dan-event', 'Berita::artikel_dan_event');
    $routes->get('figur', 'Berita::figur');
});
$routes->get('/galeri', 'Galeri::index');
$routes->get('/kontak', 'Kontak::index');

$routes->post('/subscribe', 'Others::subscribe');

/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (is_file(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
    require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
